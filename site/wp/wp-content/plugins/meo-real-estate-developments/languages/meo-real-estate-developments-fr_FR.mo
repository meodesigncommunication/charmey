��    7      �  I   �      �     �     �     �  	   �  
   �     �  	   �       	                	   )     3     ?     L     _     m  
   ~     �  	   �  Q   �  [   �     J     Z     m  
   z     �     �     �  	   �     �     �  !   �     �     �     �       !        >     M     i     v     �     �     �     �  #   �     �     �     �     �               -  �  0     )
     >
     X
     j
     y
     �
     �
     �
     �
     �
  	   �
  
   �
     �
     �
     �
               3     C     P  N   ^  ]   �                :     L     [     k     r     z     �     �      �     �     �  )   �     �  .        G  &   ]     �  #   �     �  $   �     �     �  $   �  	   $     .     6     <     H  $   L     q     6         7                                )                          "                                             %   
   (   .       &       ,   !             '   1   2   5   -          0       #   3                                  +   *   	   /   $   4    Add a building Add a development Add a floor Add a lot Add a plan All Apartment Availability Available Balcony Building Buildings Development Developments Download Info pack Edit building Edit development Edit floor Edit lot Edit plan Error: plugin "MEO Real Estate Developments" depends on the following" plugins.   Error: plugin "MEO Real Estate Developments" requires a newer version of PHP to be running. Find a building Find a development Find a floor Find a lot Find a plan Floor Floors Info pack Lot Lots Minimal version of PHP required:  No No building found No building found in the trash No development found No development found in the trash No floor found No floor found in the trash No lot found No lot found in the trash No plan found No plan found in the trash Plan Plans Please install and/or activate them Reserved Rooms Sold View lot details Yes Your server's PHP version:  p. Project-Id-Version: MEO real estate
POT-Creation-Date: 2015-05-18 13:40+0100
PO-Revision-Date: 2015-05-18 20:49+0100
Last-Translator: Peter Holberton <webdev@allmeo.com>
Language-Team: MEO <webdev@allmeo.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.7.4
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;mred_translate
X-Poedit-SearchPath-0: ..
 Ajouter un bâtiment Ajouter un développement Ajouter un étage Ajouter un lot Ajouter un plan Tous Appartement Disponibilité Libre Balcon Bâtiment Bâtiments Développement Développements Télécharger dossier complet Modifier bâtiment Modifier développement Modifier étage Modifier lot Modifier plan Erreur: extension "MEO Real Estate Developments" dépend des plugins suivants. Erreur: extension "MEO Real Estate Developments" nécessite une version plus récente de PHP. Trouver un bâtiment Trouver un développement Trouver un étage Trouver un lot Trouver un plan Étage Étages Dossier complet Lot Lots Version minimale de PHP requise: Non Aucun bâtiment trouvé Aucun bâtiment trouvée dans la poubelle Aucun développement trouvée Aucun développement trouvée dans la poubelle Aucun étage trouvée Aucun étage trouvée dans la poubelle Aucun lot trouvée Aucun lot trouvée dans la poubelle Aucun plan trouvée Aucun plan trouvée dans la poubelle Plan Plans Merci de les installer et/ou activer Réservé Pièces Vendu Voir ce lot Oui La version de PHP sur votre serveur: p. 