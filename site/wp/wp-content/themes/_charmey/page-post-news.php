<?php
/*
Template Name: Single Post News
*/

global $post;

$currentLang = qtrans_getLanguage();

$news = get_post( $_GET['id'] );

?>
<div id="content" class="container page-portfolio-php">
    <div class="simple-page-content">
        <div class="post-news-preview">
            <div class="news-featured">
                <?php if ( has_post_thumbnail( $news->ID ) ):
                    echo get_the_post_thumbnail( $news->ID, 'full' );
                 endif ?>
            </div>
            <div class="page-header">
                <h1 class="entry-title title-1"><?php echo _e($news->post_title); ?></h1>
            </div>
            <div class="news-content">
                <?php
                     echo qtrans_use($currentLang, $news->post_content, false);
                ?>
            </div>
        </div>
    </div>
</div>
</div>
