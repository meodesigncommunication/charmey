<?php //global $virtue; error_reporting(E_ALL & ~E_WARNING);

ini_set('display_errors', '1'); 

if(is_category())
{
    get_template_part('templates/category');
}else{
?>
<div id="content" class="container">
  <div class="row row-archive">
        <?php if(isset($virtue['blog_archive_full']) && $virtue['blog_archive_full'] == 'full') {
            $summery    = 'full';
            $postclass  = "single-article fullpost";
          } else {
            $summery = 'normal';
            $postclass = 'postlist';
          } ?>
    <div class="main <?php echo esc_attr(kadence_main_class()); ?>  <?php echo esc_attr($postclass);?> main-archive" role="main">

    <?php if (!have_posts()) : ?>
        <div class="alert">
          <?php _e('Sorry, no results were found.', 'virtue'); ?>
        </div>
        <?php get_search_form(); ?>
    <?php endif; ?>

    <?php if($summery == 'full') {
            while (have_posts()) : the_post();
              get_template_part('templates/content', 'fullpost');
            endwhile;
          } else {
          	$indexLoopArch = 0;
			$cropsize = 365;
			

			$cssTitle = 'title-1';
			$cssText = 'text-1';
			 
			
            while (have_posts()) : the_post(); 
            
              # get_template_part('templates/content', get_post_format());
              
              # Include code from template to solve loop index number issue
              
              $indexLoopArch++;
              if($indexLoopArch > 1){
              	$cropsize = 180;
              	$cssTitle = 'title-2';
              	$cssText  = 'text-2';
              }
              $post = get_post(get_the_ID());
               
             ?>
              		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
	                      <div class="row">
	                          <?php 
	                         
	                            $textsize = 'col-md-7'; 
	                            if (has_post_thumbnail( $post->ID ) ) {
	                                $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
	                                $thumbnailURL = $image_url[0];
	                                $image = @aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
	                                if(empty($image)) { $image = $thumbnailURL; } ?>
	                                <div class="col-md-5">
	                                    <div class="imghoverclass img-margin-center">
	                                        <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
	                                            <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
	                                        </a> 
	                                     </div>
	                                 </div>
	                                <?php $image = null; $thumbnailURL = null; 
                                } else {
                                  $thumbnailURL = virtue_post_default_placeholder();
                                  $image = @aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
                                  if(empty($image)) { $image = $thumbnailURL; } ?>
                                  <div class="col-md-5">
                                    <div class="imghoverclass img-margin-center">
                                        <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
                                            <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
                                        </a> 
                                     </div>
                                 </div>
                                <?php $image = null; $thumbnailURL = null; 
                                } ?>
	
	                      <div class="<?php echo esc_attr($textsize);?> postcontent">
	                          <div class="squarre-gradient"></div>
	                          <header>
	                              <a href="<?php the_permalink() ?>"><h2 class="entry-title <?php echo $cssTitle; ?>" itemprop="name headline"><?php the_title(); ?></h2></a>
	                               <?php // get_template_part('templates/entry', 'meta-subhead'); ?>    
	                          </header>
	                          <div class="plus-read-more"></div>
	                        </div><!-- postcontent -->
	                        
	                        <div class="<?php echo esc_attr($textsize);?> postcontent-excerpt">
	                          <header>
	                              <a href="<?php the_permalink() ?>"><h2 class="entry-title  <?php echo $cssTitle; ?>" itemprop="name headline"><?php the_title(); ?></h2></a>
	                          </header>
	                          <div class="entry-content <?php echo $cssText; ?>" itemprop="articleBody">
	                              <?php 
	                              # First Article is long desc
	                              if($indexLoopArch == 1) {
	                              	add_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
	                              	add_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
	                              }
	                              else {
	                              	remove_filter('excerpt_more', 'kadence_excerpt_more');
	                              	add_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
	                              	add_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
	                              }
	                              	the_excerpt();
		    						if($indexLoopArch == 1) {
		    							remove_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
	                              		remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
		    						}
	                              else {
	                              	remove_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
	                              	remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
	                              }
	                              ?>
	                          </div>
	                          <div class="plus-read-more"><a href="<?php the_permalink() ?>"></a></div>
	                        </div><!-- postcontent-excerpt -->
	                  </div><!-- row-->
	              </article> <!-- Article -->
              
              <?php 
              # /Include code from template
             /* */
            endwhile;
          }
          //Page Navigation
          if ($wp_query->max_num_pages > 1) :
            virtue_wp_pagenav();
          endif; ?>
		
	<?php get_template_part('templates/page-bottom'); ?>
		
    </div><!-- /.main -->
<?php }?>