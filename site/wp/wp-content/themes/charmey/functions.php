<?php 

/* Init */
	add_action('init', 'init_sessions');
	
function init_sessions() 
{
	if (!session_id()) 
	{
		session_start();
	}
}

/* Parent Theme */
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    /*wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );*/
}
	
## Image Sizes
	add_action( 'after_setup_theme', 'charmey_images_setup' );

function charmey_images_setup(){
	add_image_size('image_gallery_thumbnail', 62, 62, true);
}

# Translate MailChhimp visible fields
add_filter('mc4wp_form_content', 'charmey_mc4wp_form_content');

function charmey_mc4wp_form_content($content){
	return __($content);
}

######################
## Wishlist Page ID ##
######################
global $wishlistPageID;
$wishlistPageID = 2; // Update if needed

/* Remove Wpfp Js script if we are on the Wishlist page / Otherwise re-add it */
	add_action('wp_head', 'meo_handle_scripts', 1); // Because we have the page ID at that time
	
function meo_handle_scripts() {
	global $wp_query, $wishlistPageID;
	
	if(is_page($wishlistPageID)){
		add_action('wp_print_scripts', 'meo_deregister_scripts', 100);
		// Write updated Js script in the footer instead
		add_action('wp_footer', 'meo_wpfpjs', 100);
	}
	
}

/* Remove wpfp js script */
function meo_deregister_scripts(){
	remove_action('wp_print_scripts', 'wpfp_add_js_script');
	wp_dequeue_script('wp-favroite-posts');
}

/* Replace wpfp js script */
function meo_wpfpjs(){
	echo "<script type='text/javascript'>
	jQuery(document).ready( function($) {
		if(jQuery('.wpfp-link-full').length){
	        jQuery('.wpfp-link-full').click(function(e) {
	        	e.preventDefault();
	                dhis = jQuery('.wpfp-link-full');
	                pids = dhis.attr('data-list');
	
	                jQuery.ajax({
	                    url:'".get_option('home')."/wp/wp-admin/admin-ajax.php',
	                    type:'POST',
	                    data:'action=wpfp_import_full&pids=' + pids
	                    })
	                    .done(function(results){location.reload();});
	                
				return false;
	        });
	   }
	
		if(jQuery('.wpfp-link').length){
	        jQuery('.wpfp-link').click(function(e) {
	        	e.preventDefault();
	                dhis = jQuery(this);
	                params = dhis.attr('href').replace('?', '') + '&ajax=1';
					
	                jQuery.ajax({
	                    url:'".get_option('home')."/wp/wp-admin/admin-ajax.php',
	                    type:'POST',
	                    data:params
	                    })
	                    .done(function(results){location.reload();});
	                
				return false;
	        });
	   }
	});
	</script>";
}

/* Contact Form 7 & Wishlist */

/* Add shortcode to Wordpress */

add_shortcode( 'wishlist_4emailcontent' , 'wishlist_to_email_content' );

function wishlist_to_email_content(){

	global $favorite_post_ids, $wishlistPageID;
	
	$favorite_post_ids = wpfp_get_users_favorites();
	
	if ($favorite_post_ids) {
	
		$favorite_post_ids = array_reverse($favorite_post_ids);
	
	}
	
	$return_wishlist_url = get_permalink($wishlistPageID).'wishes/';
	foreach($favorite_post_ids as $favid){
		$return_wishlist_url.= $favid.'-';
	}
	return substr($return_wishlist_url, 0, -1); // Wishlist URL
	
}


/* Add custom shortcode to CF7 */
add_action( 'wpcf7_init', 'charmey_add_shortcode_wishlistcf7' );


function charmey_add_shortcode_wishlistcf7() {
	wpcf7_add_shortcode( 'wishlistcf7', 'charmey_add_shortcode_wishlistcf7_handler' );
}

function charmey_add_shortcode_wishlistcf7_handler( $tag ) {
	return do_shortcode('[wishlist_4emailcontent]');
}


/* Wishlist shortcode in email */
add_filter( 'wpcf7_special_mail_tags', 'wishlist_mail_tag', 10, 3 );

function wishlist_mail_tag( $output, $name, $html ) {
	if ( '_wishlistcf7' == $name )
		$output = do_shortcode( '[wishlist_4emailcontent]' );

	return esc_html($output);
}

/* Excerpt */
function custom_excerpt_length_long( $length ) {
	return 80;
}
function custom_excerpt_length_short( $length ) {
	return 10;
}
	/* Length filters are not called when excerpt field is not empty */
function custom_cut_excerpt_length_long( $excerpt ) {
	$arrayOfwords = explode(' ', $excerpt);
	$text = '';
	if(count($arrayOfwords) > 80){
		for($i=0; $i<=80; $i++){
			$text .= $arrayOfwords[$i].' ';
		}
		return $text.'&hellip;';
	}
	
	return $excerpt;
}
function custom_cut_excerpt_length_short( $excerpt ) {
	$arrayOfwords = explode(' ', $excerpt);
	$text = '';
	if(count($arrayOfwords) > 10){
		for($i=0; $i<=10; $i++){
			$text .= $arrayOfwords[$i].' ';
		}
		return $text.'&hellip;';
	}
	
	return $excerpt;
}

/* Remove URL field in comment form */
function disable_comment_url($fields) {
    unset($fields['url']);
    return $fields;
}
add_filter('comment_form_default_fields','disable_comment_url');

/* Wishlist / Favorite posts */
	add_filter( 'the_content', 'excerpt_favorite_shortcode' );

function excerpt_favorite_shortcode($content){
	
	if (function_exists('wpfp_link')) { 
		
		/* Add it to posts only, and when they appear on a page that list them 
		 * to avoid to see them twice 
		 */
		if (!is_front_page() && !is_single() && $GLOBALS['post']->post_type == 'post') {
			$content.= wpfp_link(1);
		}
	
	}
  	
	return $content;
}

/* Wishlist AJAX function to add multiple items at a time */
	add_action('wp_ajax_wpfp_import_full', 'meo_wpfp_import_full');
	add_action('wp_ajax_nopriv_wpfp_import_full', 'meo_wpfp_import_full');

function meo_wpfp_import_full(){
	
	if(isset($_POST['pids']) && function_exists('wp_favorite_posts'))
	{
		
		// Extract items ID
		$items = explode('-', $_POST['pids']);
		
		foreach($items as $item){
				
			if(is_numeric($item)){
				
				$wish = get_post($item);
				// Work only with published items
				if($wish->post_status == 'publish'){
					
        			/* Check if item has already been favorited */
        			if(!wpfp_check_favorited($wish->ID)){
        				
	        			if (is_user_logged_in()) {
					        wpfp_add_to_usermeta($wish->ID);
					    } else {
					        wpfp_set_cookie($wish->ID, "added");
					    }
        			}
				}
			} // End is_num
		} // End foreach
	} // End isset
	die();
}

/* Wishlist add Facebook / Open Graph meta tags */
	add_action('wp_head', 'wishlist_open_graph_meta', 100);
	
function wishlist_open_graph_meta(){
	
	// MyWishList page id
	global $wishlistPageID;
	
	if(is_page($wishlistPageID)) {
		
		$initPostIdsURL = get_permalink($wishlistPageID);
		
		// Get Wishlist params if we are on the Wishlist page
		global $user, $favorite_post_ids;
		// -> Can t work / access these global params, so we get them trhough function
		
		$favorite_post_ids = wpfp_get_users_favorites();
		
	    if ($favorite_post_ids) {
	    	
			$favorite_post_ids = array_reverse($favorite_post_ids);
	        $post_per_page = wpfp_get_option("post_per_page");
	        $page = intval(get_query_var('paged'));
			
	        // To present only the published ones, in case they were unpublished by the time we present the Wishlist page
	        $qry = array('post__in' => $favorite_post_ids, 
	        				'posts_per_page'=> $post_per_page, 
	        				'orderby' => 'post__in', 
	        				'paged' => $page,
	        				'post_status' => 'publish');
	        // custom post type support can easily be added with a line of code like below.
	        // $qry['post_type'] = array('post','page');
	        /* Sort by Category
	        add_filter('posts_join', create_function('$a', 'global $wpdb; return $a . " INNER JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id) INNER JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id) ";'));
			add_filter('posts_where', create_function('$a', 'global $wpdb; return $a . " AND $wpdb->term_taxonomy.taxonomy = \'category\'";'));
			add_filter('posts_orderby', create_function('$a','global $wpdb; return "$wpdb->term_taxonomy.term_id ASC";'));
	         */
	       	$the_query = new WP_Query( $qry );
	        
		    $initPostIdsURL.= 'wishes/';
	        
	        while ( $the_query->have_posts() ) : $the_query->the_post();
	        	
				/* Add it to the Shared URL */
				$initPostIdsURL.= get_the_ID().'-';
	            
	        endwhile;
	        
	    }
	    wp_reset_postdata();
	    
		echo '
		<meta property="og:url"         content="'.substr($initPostIdsURL, 0, -1).'" />
	    <meta property="og:title" 		content="Charmey Wishlist" /> 
	    <meta property="og:image" 		content="'.get_stylesheet_directory_uri().'/images/wishlist.jpg" /> 
	    <meta property="og:description" content="I share my Wishlist on Charmey with you!" />';
	}
}


	
function wishlist_canonical(){
	echo "<link rel='canonical' href='".$_SERVER['REDIRECT_SCRIPT_URI']."' />";
}

// remove page post type from tag list
function fb_search_filter($query) {
	# Override Search mechanism
	if($query->is_search){
		
		$query->set('post_type', 'post' );
		if(isset($_GET['tags']) && $_GET['tags'] != ''){
			$tags = substr($_GET['tags'], 0, -1);
			$query->set('tag__in', explode('-', $tags) );
		}
	}
	# Override Archives
	if ( $query->is_archive) {
		$query->set('post_type', 'post' );
	}
	# Override category
	if ( $query->is_category) {
		$query->set( 'posts_per_page', 1000 );
	}
	return $query;
}
add_filter( 'pre_get_posts', 'fb_search_filter' );

function meo_template_redirect() {
	
	// Tests will be made directly in the template, to centralize the code <------- NOOOOOOOOOOOOOOOOOOO ! as we will modify the wp_query
	if (isset($_GET['wishes'])) {
		
		$wishes = $_GET['wishes'];
		
		if(isset($_GET['lang']) && $_GET['lang'] != '')
		{
			global $q_config;
			$q_config['q_config']['language'] = $_GET['lang'];
		}
		
		// Redifining the query to avoid 404 and get correct post if exists
		global $wp_query;
		
		$old_wp_query = $wp_query;
		$wp_query = new WP_Query('p=2&post_status=publish&post_type=page');
		$wp_query->is_page = 1;
		
		// Check if we have a news from user, yes ok, no 404
		if($wp_query->have_posts())
		{
			/* Wishlist add canonical url */
			add_action('wp_head', 'wishlist_canonical', 100);
			
			header("HTTP/1.1 200 OK");
			header("Status: 200 OK");
			include (STYLESHEETPATH . '/wpfp-page-template-full.php');
			exit;
		}
		else $wp_query = $old_wp_query;
		
	}
	else
	{
		// 404
	}
       
}
add_action('template_redirect', 'meo_template_redirect');

/* Smart Capture */
/* Shortcode to generate located CalltoAction buttons [call2actionbtn title | url | target | class | linkclass ]
 * [call2actionbtn title="" url="" target="" class=""] */
	add_shortcode( 'call2actionbtn', 'call2action_shortcode' );

  	function call2action_shortcode( $atts ) {
	    $a = shortcode_atts( array(
	        'title' => '',
	        'url' => '#',
	    	'target' => '_self',
	    	'class' => '',
	    	'linkclass' => '',
	    ), $atts );

	    return '<div class="c2a-link-single '.$a['class'].'"><a href="'.$a['url'].'" target="'.$a['target'].'" class="'.$a['linkclass'].'">'.$a['title'].'</a></div>';
	}
	
	####
	## Emulating MEO Realestate functions to auto generate links
	####
	// Get download button HTML
	function mred_get_download_button($lot_id) {
		global $download_lot_id;
		$download_lot_id = $lot_id;
	
		$lot = mred_get_lot($lot_id);
		if (!empty($lot['pdf'])) {
			return '<a class="fancybox-iframe pdf-download-link" href="' .  mred_get_pdf_request_url($lot_id) . '">' . mred_translate('Download Info pack') .'</a>';
		}
	
		$lot_link = get_permalink($lot_id);
		return '<a class="see-lot-link" href="' . $lot_link .'"><span class="dl-plan-typo1">' . __('[:fr]T&eacute;l&eacute;charger[:en]Download[:de]Download') .'</span> <span class="dl-plan-typo2">' . __('[:fr]dossier complet[:en]complete file[:de]complete file') .'</span></a>';
	}
	function mred_get_pdf_request_url($lot_id, $additional_parameters = '') {
		$lot = mred_get_lot($lot_id);
		if (empty($lot)) {
			return '#';
		}
		$pdf = $lot['pdf'];
		if (empty($pdf)) {
			$plan = mred_get_plan($lot['plan_id']);
			if (empty($plan)) {
				return '#';
			}
			$pdf = $plan['pdf'];
		}

		if (!empty($pdf) && isset($pdf['id'])) {
			// Use get_permalink, rather than $pdf['url'], to allow filter hooks to run
			$result = get_permalink($pdf['id']);
			if (!empty($additional_parameters)) {
				$result .= ( strpos($result, '?') === false ) ? '?' : '&';
				$result .= $additional_parameters;
			}
			return $result;
		}

		return '#';
	}

#############################################
/* Configure Advanced Custom Fields plugin */
#############################################
define('ACF_OPTION_PAGE_STUB', 'acf-options-charmey-settings');

if( function_exists('acf_add_options_page') ) {
	// Note - if changing this, the ACF_OPTION_PAGE_STUB will need to change
	acf_add_options_page('Charmey Settings');
}

if( function_exists('register_field_group') ) {

	register_field_group(array (
		'key' => 'group_54dccd889a11f',
		'title' => 'Charmey Settings',
		'fields' => array (
			array (
				'key' => 'field_54dccdaa6f49f',
				'label' => 'Logo',
				'name' => 'logo',
				'prefix' => '',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'medium',
				'library' => 'all',
			),
			array (
				'key' => 'field_54dccddc6f4a0',
				'label' => 'Adresse',
				'name' => 'address',
				'prefix' => '',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 4,
				'new_lines' => 'br',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_54dcce096f4a1',
				'label' => 'Phone',
				'name' => 'phone',
				'prefix' => '',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_54dcce126f4a2',
				'label' => 'email',
				'name' => 'email',
				'prefix' => '',
				'type' => 'email',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
			array (
				'key' => 'field_54dcce1b6f4a3',
				'label' => 'Adresse web',
				'name' => 'url',
				'prefix' => '',
				'type' => 'text',
				'instructions' => 'Without http://',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => ACF_OPTION_PAGE_STUB,
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));
}

/* Add a CSS wp-admin.css pour cacher les menu du plugin real-estate */
function my_custom_css() {    
    echo '<link rel="stylesheet" href="'.get_stylesheet_directory_uri().'/wp-admin.css" type="text/css" media="all" />';  
}
add_action('admin_head', 'my_custom_css');

function shortcodes_in_cf7( $form ) {
    $form = do_shortcode( $form );
    return $form;
}
add_filter( 'wpcf7_form_elements', 'shortcodes_in_cf7' );

/* Shortcode to generate input hidden [inputPreviousUrl url ]
 * [inputPreviousUrl url=""] */
	add_shortcode( 'inputPreviousUrl', 'inputPreviousUrl_shortcode' );

  	function inputPreviousUrl_shortcode() {
            $url = '';
            if(isset($_GET['current_url']) && !empty($_GET['current_url']))
            {
                $url = 'http://'.$_GET['current_url'];
            }
	    return '<input type="hidden" name="pervious_url" value="'.$url.'">';
	}        
/* Shortcode to generate button cancel for file request page on mobile device [buttonCancelPreviousUrl url ]
 * [buttonCancelPreviousUrl url=""] */
        add_shortcode( 'buttonCancelPreviousUrl', 'buttonCancelPreviousUrl_shortcode' );

  	function buttonCancelPreviousUrl_shortcode() {
            
            $url = '';
            $html = '';
            
            if(isset($_GET['current_url']) && !empty($_GET['current_url']))
            {
                $url = $_GET['current_url'];
                $html = '<a class="wpcf7-button-cancel" href="'.$url.'"><input class="wpcf7-form-control wpcf7-submit" type="button" value="Annuler" /></a>';
            }
            
            return $html;
	}
      
/**
 * This function will connect wp_mail to your authenticated
 * SMTP server. This improves reliability of wp_mail, and 
 * avoids many potential problems.
 *
 * Author:     Chad Butler
 * Author URI: http://butlerblog.com
 *
 * For more information and instructions, see:
 * http://b.utler.co/Y3
 */
// add_action( 'phpmailer_init', 'send_smtp_email' );
function send_smtp_email( $phpmailer ) {

	// Define that we are sending with SMTP
	$phpmailer->isSMTP();

	// The hostname of the mail server
	$phpmailer->Host = "mail.infomaniak.ch";

	// Use SMTP authentication (true|false)
	$phpmailer->SMTPAuth = true;

	// SMTP port number - likely to be 25, 465 or 587
	$phpmailer->Port = "587";

	// Username to use for SMTP authentication
	$phpmailer->Username = "info.tourisme@charmey.ch";

	// Password to use for SMTP authentication
	$phpmailer->Password = "w3bEst@!mail!";

	// Encryption system to use - ssl or tls
	$phpmailer->SMTPSecure = "tls";

	$phpmailer->From = "no-reply@charmey.ch";
	$phpmailer->FromName = "Charmey Tourisme";
	
	// print_r($phpmailer);
}

function my_custom_post_types_post_news() {
    $labels = array(
        'name'               => 'News',
        'singular_name'      => 'News',
    );
    $args = array(
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt'),
        'public'      => true,
        'labels'      => $labels,
    );
    register_post_type( 'post_news', $args );
}
add_action( 'init', 'my_custom_post_types_post_news' );

/**
 * Register our sidebar and widgetized area.
 *
 */
function home_weather_widget_init() {
    register_sidebar( array(
        'name'          => 'Home Weather Area',
        'id'            => 'home_weather_area',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );
}
function newsroom_widget_init() {
    register_sidebar(array(
        'name' => 'Newsroom Widget',
        'id' => 'widget_newsroom',
        'before_widget' => '<div class="r-share" id="widget_newsroom">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => '',
    ));
}
function navigation_1_widget_init() {
    register_sidebar( array(
        'name'          => 'Navigation 1',
        'id'            => 'navigation_1',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );
}
function navigation_2_widget_init() {
    register_sidebar( array(
        'name'          => 'Navigation 2',
        'id'            => 'navigation_2',
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'home_weather_widget_init' );
add_action( 'widgets_init', 'newsroom_widget_init' );
add_action( 'widgets_init', 'navigation_1_widget_init' );
add_action( 'widgets_init', 'navigation_2_widget_init' );