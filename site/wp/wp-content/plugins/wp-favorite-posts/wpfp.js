jQuery(document).ready( function($) {
    $('.wpfp-link').live('click', function() {
        dhis = $(this);
        wpfp_do_js( dhis, 1 );
        // for favorite post listing page
        if (dhis.hasClass('remove-parent')) {
            dhis.parent("li").fadeOut();
        }
        console.log(dhis.attr('data-action'));
        if(dhis.attr('data-action') == 'add'){
        	// $('#wishlist-counter').html('+1');
        	 $('#wishlist-counter').fadeIn(1000);
             /*$('.tcb-menu-wishlist, .tcb-menu-wishlist a, .tcb-menu-wishlist a img').animate({ "width": "24px", "height":"18px"}, "slow");
             $('.tcb-menu-wishlist, .tcb-menu-wishlist a, .tcb-menu-wishlist a img').animate({ "width": "20px", "height":"16px"}, "slow");*/
             $('#wishlist-counter').fadeOut(3500);
        }
        else {
        	// $('#wishlist-counter').html('-1');
        }
        return false;
    });
});

function wpfp_do_js( dhis, doAjax ) {
    loadingImg = dhis.prev();
    loadingImg.show();
    beforeImg = dhis.prev().prev();
    beforeImg.hide();
    url = document.location.href.split('#')[0];
    params = dhis.attr('href').replace('?', '') + '&ajax=1';
    if ( doAjax ) {
        jQuery.get(url, params, function(data) {
                dhis.parent().html(data);
                if(typeof wpfp_after_ajax == 'function') {
                    wpfp_after_ajax( dhis ); // use this like a wp action.
                }
                loadingImg.hide();
            }
        );
    }
}
