<?php
/*
Template Name: Tags/Mots Clefs Activites
*/

$categories = get_categories();

/* 
[term_id] => 38
            [name] => Bien-�tre
            [slug] => bien-etre
            [term_group] => 0
            [term_taxonomy_id] => 38
            [taxonomy] => category
            [description] => Havre de paix et de repos, Les bains de la Gruy�re situ� � Charmey vous convient dans un espace de d�tente et de relaxation. Que l�esprit vagabonde dans une exposition du Mus�e de Charmey reconnu par sa Triennale du Papier ou s��chappe dans une balade � travers les pr�s, il se ressource du plaisir et de la joie de partager des instants de convivialit� entre amis ou en famille. Ponctu� d�un t�te � t�te dans l�une des tables de la vall�e, vos batteries seront recharg�s pour faire face au rythme intr�pide du quotidien.
            [parent] => 0
            [count] => 1
            [cat_ID] => 38
            [category_count] => 1
            [category_description] => Havre de paix et de repos, Les bains de la Gruy�re situ� � Charmey vous convient dans un espace de d�tente et de relaxation. Que l�esprit vagabonde dans une exposition du Mus�e de Charmey reconnu par sa Triennale du Papier ou s��chappe dans une balade � travers les pr�s, il se ressource du plaisir et de la joie de partager des instants de convivialit� entre amis ou en famille. Ponctu� d�un t�te � t�te dans l�une des tables de la vall�e, vos batteries seront recharg�s pour faire face au rythme intr�pide du quotidien.
            [cat_name] => Bien-�tre
            [category_nicename] => bien-etre
            [category_parent] => 0
 */
?>
<div id="content" class="container page-fullwidth-php">
   	<div class="row row-archive">
     	<div class="main col-md-12 postlist what-to-do" role="main">
            <?php get_template_part('templates/page', 'header'); ?>
            <?php 
            
            foreach($categories as $c => $catObj){
            	$link = get_home_url('/').'/activites/'.$catObj->slug;
            ?>
            	<article id="post-category" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
                	<div class="row">
            			<div class="col-md-5">
                            <div class="imghoverclass img-margin-center">
                                <a href="<?php echo $link;  ?>" title="<?php echo $catObj->name; ?>">
                                    <img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/shape<?php echo rand(1,2); ?>.png" alt="<?php echo $catObj->name; ?>" class="iconhover" style="display:block;">
                                </a> 
                            </div>
                        </div>
                   		<div class="col-md-7 postcontent">
                          <header>
                              <a href="<?php echo $link; ?>"><h2 class="entry-title" itemprop="name headline"><?php echo $catObj->name; ?></h2></a>
                               <?php // get_template_part('templates/entry', 'meta-subhead'); ?>    
                          </header>
                          <div class="plus-read-more"><a href="<?php echo $link; ?>"><?php echo $catObj->count; ?></a></div>
                        </div><!-- Text size -->
                        
                        <div class="col-md-7 postcontent-excerpt">
                          <header>
                              <a href="<?php echo $link; ?>"><h2 class="entry-title" itemprop="name headline"><?php echo $catObj->name; ?></h2></a>
                          </header>
                          <div class="entry-content" itemprop="articleBody">
                              <?php // echo $catObj->description; ?>
                          </div>
                          <div class="plus-read-more"><a href="<?php echo $link; ?>"><?php echo $catObj->count; ?></a></div>
                        </div><!-- Text size -->
                  </div><!-- row-->
              </article> <!-- Article -->
            <?php 
            } ?>
        </div><!-- /.main -->