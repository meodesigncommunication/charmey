<?php
/*
Template Name: Test Snow Data
*/

$url = 'http://snow.myswitzerland.com/bulletin_enneigement/FribourgRegion/Charmey-4';

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$data = curl_exec($ch);

$search = array('/>', '>', '<', 'em');
$replace = array('', '', '', '');

$dataPiste = substr($data, 22496, -47181);
$distanceOpen = explode('/', $dataPiste);

$dataInstallations = substr($data, 22652, -47025);
$installationsOpen = explode('/', $dataInstallations);

if(!empty($distanceOpen[0]))
{
    $distance = str_replace($search, $replace, $distanceOpen[0]);
}else{
    $distance = str_replace($search, $replace, $distanceOpen[1]);
}

if(!empty($installationsOpen[0]))
{
    $installations = str_replace($search, $replace, $installationsOpen[0]);
}else{
    $installations = str_replace($search, $replace, $installationsOpen[1]);
}

echo '<p>' . $distance . '/30 km' . '</p>';
echo '<p>' . $installations . '/6' . '</p>';

?>