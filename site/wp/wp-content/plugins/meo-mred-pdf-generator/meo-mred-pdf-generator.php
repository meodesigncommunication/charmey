<?php
/*
Plugin Name: MMPG - MEO MRED PDF / JS/TXT Files Generator
Version: 1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sarl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch

- Constants
- Dependencies
- PDF Creation
- JS/TXT Creation
- Auto Update Action Hooks
- Common functions


	#######################################################################################
	## /!\ Comment add_action( 'wp_enqueue_scripts', 'mred_localise' );              /!\ ##
	## /!\ in meo-real-estate-developments.php in meo-real-estate-development plugin /!\ ##
	#######################################################################################


*/
require_once( $plugin_root . 'fpdf/fpdf.php' );
require_once( $plugin_root . 'class.pdfcreator.php' );

define('UPLOADS_DESTINATION', dirname(__FILE__).'/../../uploads/');
define('PDF_DESTINATION', dirname(__FILE__).'/../../uploads/pdf/');

function generate_fpdf_post_save($post = null)
{
    // Variable utile
    global $q_config;
    $languages = $q_config['enabled_languages'];
    $currentLang = qtrans_getLanguage();
    
    if(empty($post->ID))
    {
        $post_id = get_the_ID();
        $post = get_post($post_id);
    }else{
        $post_id = $post->ID;
    }
    
    foreach($languages as $lang)
    {
        // Set la langue $lang
        //qtrans_setLanguage($lang);      
        
        $pdf = new PDFCreator();
        
        // Elements du poste à afficher
        $title = $pdf->convertStringPDFUtf8Decode(__($post->post_title));
        $content = __($post->post_content); // $pdf->convertStringPDFUtf8Decode(__($post->post_content));
        if(is_array(get_post_meta($post_id, 'infos_texte'))){
        	$infoText = $pdf->convertStringPDFUtf8Decode( __(reset(get_post_meta($post_id, 'infos_texte'))));
        }
        else $infoText = '';
        
        
        /*echo $title;
        echo $content;
        echo $infoText;
        
        exit();*/
        
        //$infoText = $pdf->convertStringPDFUtf8Decode(__(get_post_meta($post_id, 'infos_texte')));
        
        
        // Début de la création du pdf  
        $pdf->initPagePDF();        
        $pdf->Ln(10); 

        // Titre du poste
        $pdf->SetDrawColor(254,212,64);
        $pdf->Cell(0,13,$title,'B'); 
        $pdf->Ln(18); 
        
        // Function qui va récupérer le chemin de l'image du post ainsi que ca taille
        $fileAttachment = $pdf->generateTabFileAttachment(get_post_thumbnail_id($post_id),[300,300]);
        
        // Condition qui check si une image est lié
        if(!empty($fileAttachment['path']) && file_exists($fileAttachment['path']))
        {
            // Taille du retour à la ligne selont taille de l'image
            $sizeLn = round((($fileAttachment['height']*1)*$pdf->convertPxToMm)+5);  
            
            // Affichage de l'image dans le PDF
            $pdf->image($fileAttachment['path'], $pdf->GetX(),$pdf->GetY());   
            
            // Retour à la ligne dans le pdf
            $pdf->Ln($sizeLn);
        }       
        
        $tabContent = $pdf->generateArrayImageGalleryContent($content);
        if($tabContent['content']){
        	$content = $pdf->convertStringPDFUtf8Decode($tabContent['content']);
        }
        $tabImageContent = $tabContent['imageID']; 
        
        // Afficher le contenu du poste
        $pdf->SetFont('Arial','',12);
        $pdf->MultiCell(0,6,$content); 
        $pdf->Ln(5); 
        
        // Affiche les images du contenu dans le pdf
        $pdf->insertImageContent($tabImageContent);

        // Affiche les information pratique dans le pdf
        $pdf->insertSpecialInfo($infoText);
        
        // Enregistrement du pdf créé ci-dessus
        // Nom du fichier = Charmey_info_+substr(md5($post_id."_".qtrans_getLanguage()),5,5).pdf
        //$pdf->Output(PDF_DESTINATION.'Charmey_info_'.substr(md5($post_id."_".qtrans_getLanguage()),5,5).'.pdf','F');
        $pdf->Output(PDF_DESTINATION.'Charmey_info_'.$post_id."_".$lang.'.pdf','F');
    }
}
add_action('save_post','generate_fpdf_post_save');

function mmpg_generate_scripts_callback()
{
    $i = 0;
    $a = 0;
    $error = array();
    
    $args = array(
        'posts_per_page' => -1,
        'post_type' => 'post',
        'post_status' => 'publish'
    );
    
    $myposts = get_posts($args);
    
    foreach($myposts as $post):
        $error[] = generate_fpdf_post_save($post);
        $i++;
    endforeach;
    
    //indique le nombre de pdf de post généré (x3 pour les 3 langues) 
    echo ($i*3)." pdf on été généré (DE, FR, EN)";
    die();
}

# Uncomment below if you want the admin button to generate the file manually
	
/* Add Btn to Generate Manually the script file */
/**
 * This allows to avoid to query multiple times, per page, the database
 * The file contains the generated code once, and can be updated manually
 * in the admin section
 * 
 * Btn is in the admin footer
 * Script file is under the wp-content/uploads/cache folder
 * 
 */
add_action('admin_footer', 'generate_script_button');
function generate_script_button() { ?>
        <script type="text/javascript" >
        jQuery(document).ready(function($) {

                if(jQuery('#generate-script').length){
                        var gs = jQuery('#generate-script');
                        gs.click(function(e){
                                e.preventDefault();
                                var data = {
                                        'action': 'generate_script'
                                };

                                // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
                                $.post(ajaxurl, data, function(response) {
                                        alert('Resultat: ' + response);
                                });
                        });
                }
        });
        </script>
        <style>
        <!--
        #script-generation {
          background: none repeat scroll 0 0 #f7a770;
          bottom: 0;
          clear: both;
          display: none;
          height: 30px;
          padding: 5px 0 15px;
          position: absolute;
          right: 0;
          text-align: center;
          width: 400px;
        }
        #script-generation a {color:#fff;font-weight: bold;}
        -->
        </style>
        <div id='script-generation'>
                <p>
                        <a href='#' class='sg' id='generate-script'>R&eacute;g&eacute;n&eacute;rer le fichier PDF des articles</a>
                </p>
        </div>
        <?php 
}
add_action( 'wp_ajax_generate_script', 'mmpg_generate_scripts_callback' );
/* */