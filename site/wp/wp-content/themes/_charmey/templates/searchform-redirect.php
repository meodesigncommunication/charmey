<?php 
# Tags list (#Couples, #Familles, #Groupes Et Entreprises
$tagsList = array(30, 29, 31);

$class="searchform";
if(is_front_page() || is_home()){
	$class="searchform searchhomepage";
}
?>
<script>

    jQuery(document).ready(function(){
    	 jQuery("form#searchform").submit(function(e){
             e.preventDefault(e);
             searchFormIntercepter();
             return false;
         });
    	 jQuery("form#searchform #s").focus(function(){
             jQuery(this).val('');
             return false;
         });
    });

    function showHideSearch(){
        var display = jQuery("#top-content-searchbar").css("display");
        
        if ( display == "block" ) {
        		jQuery( "#top-content-searchbar" ).slideUp(500);
        	} else {
        		jQuery( "#top-content-searchbar" ).slideDown(500);
        	}
    }

	function searchFormIntercepter() {
	    var tagsurl = '';
	    var svalue = jQuery('input#s').val();
	    var surl = svalue.replace(' ', '+');
	    jQuery.each(jQuery("input[name='tags']:checked"), function() {
	      tagsurl+= jQuery(this).val()+'-';
	    });

		var searchurl = '<?php echo esc_url( home_url( '/' ) ); ?>';

		if(surl != ''){
			searchurl+= '?s='+surl;
		}
		if(tagsurl != ''){
			searchurl+= '&tags='+tagsurl;
			// Remove '-' at the end
			var urlLength = searchurl.length
			searchurl = searchurl.substring(0, (urlLength - 1));
		}
	    window.location.replace(searchurl);
	    return false;
	}
</script>

<div id="top-content-searchbar" class="<?php echo $class; ?>">
	<form id="searchform" method="get">
	<input type="text" name="s" value="Recherche" id="s" class="searchfield" /> <input type="submit" value="OK" class="searchsubmit" />
		<ul id="search-tags">
    	<?php foreach($tagsList as $tagID){
    		$currentTag = get_term_by('id', $tagID, 'post_tag'); ?>
    		<li><input type="checkbox" class="check-tag-<?php echo $tagID; ?>" name="tags" value="<?php echo $tagID; ?>" /><span class="title-3 dark-brown"> <?php echo __($currentTag->name); ?></span></li>
    	<?php } ?>
    	</ul>
	</form>
</div>