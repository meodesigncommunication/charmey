<?php 
# Tags list (#Couples, #Familles, #Groupes Et Entreprises
$tagsList = array(30, 29, 31);

$favorite_post_ids = wpfp_get_users_favorites();
$imageHeart = 'heart_brown_18x21_retina.png';
if(count($favorite_post_ids) > 0){
	$imageHeart = 'fullheart_brown_18x21_retina.png';
}
?>
<?php global $virtue; ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/slider/js/jquery.coda-slider-3.0.min.js"></script>
<script>
	jQuery(function(){

      /* Here is the slider using default settings */
      jQuery('#slider-id').codaSlider({
    	   slideEaseDuration: 500,
    	   dynamicArrows: false
           <?php /* dynamicArrowLeftText: "&#171;",
           dynamicArrowRightText: "&#187;"*/ ?>
          });
      jQuery('#slider-id .panel-wrapper > p').remove();
      jQuery('#slider-id h2.slide-tag').remove();
    });
</script>
<div id="top-content-bar-bg"></div>
<div id="content" class="container home-sliding">
  <div class="row row-archive">
		<div class="tcb-menu-wishlist">
            <a href="<?php echo get_page_link(2); ?>" title="Ma liste d'envie" ><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/<?php echo $imageHeart; ?>" alt="wishlist" /></a>
        </div>
		<div id="topbar-search" class="topbar-widget">
            <?php if(kadence_display_topbar_widget()) { 
            	if(is_active_sidebar('topbarright')) { dynamic_sidebar('topbarright'); } 
              } ?>
            <a id="news_nav" href="<?php echo get_page_link(5465); ?>" title="<?php echo __('[:fr]Actualit&eacute;[:en]News[:de]News'); ?>" ><?php echo __('[:fr]Actualit&eacute;[:en]News[:de]News'); ?></a>
            <a target="_blank" id="webcam_nav" href="http://www.telecabine-charmey.ch/fr/pratique--webcams.html" title="<?php echo __('[:fr]Webcams[:en]Webcams[:de]Webcams'); ?>" ><?php echo __('[:fr]Webcams[:en]Webcams[:de]Webcams'); ?></a>
        </div>
        <div class="tcb-menu-mainmenu">
			<?php /*<a id="jump-top" href="#body" class="menu-toggle show-m"></a>*/?>
			<a id="close-down" href="#html" class="menu-toggle hide-m"></a>
			<nav id="primary-navigation" class="main-nav" role="navigation">
			<?php wp_nav_menu( array( 'menu' => 'Main Menu') ); ?>
			</nav>
			<div class="tcb-menu-search"><a href="#" title="Recherche" onclick="showHideSearch();"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/search_brown_18x21_retina.png" alt="recherche" /></a></div>
		</div>
		<?php 
		get_template_part('templates/searchform-redirect', get_post_format());
		?>
        <?php if(isset($virtue['blog_archive_full']) && $virtue['blog_archive_full'] == 'large') {
            $summery    = 'full';
            $postclass  = "single-article fullpost";
          } else {
            $summery = 'normal';
            $postclass = 'postlist';
          } ?>
    <div class="main <?php echo esc_attr(kadence_main_class()); ?>  <?php echo esc_attr($postclass);?>" role="main">
    	<div id="home-page-top-text" class="title-0 dark-brown"><?php echo __('[:fr]Voici notre s&eacute;lection pour..[:en]Here is our selection for..[:de]Here is our selection for..'); ?>.</div>
    <?php /*
    <div id="top-nav">
    	<ul id="slider-top-nav">
    	<?php foreach($tagsList as $tagID){
    		$currentTag = get_term_by('id', $tagID, 'post_tag'); ?>
    		<li class="menu-item-li-<?php echo $tagID; ?>"><?php echo __($currentTag->name).' - '.$tagID; ?></li>
    	<?php } // /foreach tagsList ?>
    	</ul>
    </div>*/ ?>
<div class="coda-slider"  id="slider-id">
    <?php 
		# Foreach Tag, get 9 items, and create a slide#page with them
		foreach($tagsList as $tagID){
			
			$index = 0; // Article # index
			
			$cropsize = 365;
			
			# Sticky Posts first: return sticky posts if any, or, all posts
			$sticky = get_option( 'sticky_posts' );
		    
		    $args_sticky = array(
		        'posts_per_page' => 9,
		        'post__in'  => $sticky,
		    	'tag__in' => array($tagID)
		    );
		
		    $the_query_sticky = new WP_Query($args_sticky);
		    
		    $nbrSticky = count($the_query_sticky->posts);
		    $remainingPosts = 9 - $nbrSticky;
		    
		    # Tag info
		    $currentTag = get_term_by('id', $tagID, 'post_tag');
		    
	    	echo '<div><h2 class="title slide-tag title-3 dark-brown">'.__($currentTag->name).'</h2><p>';
	    	
		    if ( $nbrSticky > 0 ) {
		    	
		    	$cssTitle = 'title-1';
		    	$cssText = 'text-1';
		    	
                    while ($the_query_sticky->have_posts()) : $the_query_sticky->the_post(); 
                        $index++;
                        /*if($index == 2){ ?>
                            <article id="post-special" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
                                <div class="row">
									<img src="<?php bloginfo('stylesheet_directory'); ?>/images/binvinyete2_180x180_retina.jpg" alt="Charmy" />
                                </div><!-- row-->
                            </article> <!-- Article -->
                        <?php 
                        	// continue;
                        }*/
                        if($index > 1){
                        	$cropsize = 180;
                        	$cssTitle = 'title-2';
                        	$cssText = 'text-2';
                        	 
                        }
                      ?>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
                              <div class="row">
                                  <?php global $post;
                                    $textsize = 'col-md-7'; 
                                    
                                    if (has_post_thumbnail( $post->ID ) ) {
                                        $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); 
                                        $thumbnailURL = $image_url[0]; 
                                    }
                                    else {
                                        $thumbnailURL = virtue_post_default_placeholder();
                                    }
                                    $image = aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
                                    
                                    if(empty($image)) { $image = $thumbnailURL; } ?>
                                    <div class="col-md-5">
                                        <div class="imghoverclass img-margin-center">
                                            <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
                                               <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
                                            </a> 
                                        </div>
                                    </div>
                                    <?php $image = null; $thumbnailURL = null; ?>

                              <div class="<?php echo esc_attr($textsize);?> postcontent">
	                          	  <div class="squarre-gradient"></div>
                                  <header>
                                      <a href="<?php the_permalink() ?>"><h2 class="entry-title <?php echo $cssTitle; ?>" itemprop="name headline"><?php the_title(); ?></h2></a>
                                       <?php // get_template_part('templates/entry', 'meta-subhead'); ?>    
                                  </header>
                                </div><!-- postcontent -->

                                <div class="<?php echo esc_attr($textsize);?> postcontent-excerpt">
                                  <header>
                                      <a href="<?php the_permalink() ?>"><h2 class="entry-title <?php echo $cssTitle; ?>" itemprop="name headline"><?php the_title(); ?></h2></a>
                                  </header>
                                  <div class="entry-content <?php echo $cssText; ?>" itemprop="articleBody">
                                      <?php 
                                      # First Article is long desc
                                      if($index == 1) {
                                        add_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
                                        add_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
                                      }
                                      else {
                                        remove_filter('excerpt_more', 'kadence_excerpt_more');
                                        add_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
                                        add_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
                                      }
                                      
                                      echo the_excerpt();
                                      
                                      if($index == 1) {
                                         remove_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
                                         remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
                                      }
                                      else {
                                        remove_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
                                        remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
                                      }
                                      ?>
                                  </div>
                                  <div class="plus-read-more"><a href="<?php the_permalink() ?>"></a></div>
                                </div><!-- postcontent-excerpt -->
                          </div><!-- row-->
                      </article> <!-- Article -->

                    <?php 
                      endwhile;
                }
                    wp_reset_postdata();

                    # Non sticky
                    if($remainingPosts > 0){
                        $args_nonsticky = array(
                            'posts_per_page' => $remainingPosts,
                            'post__not_in' => $sticky,
                            'tag__in' => array($tagID)
                        );
                        $the_query_nonsticky = new WP_Query($args_nonsticky);

                        while ($the_query_nonsticky->have_posts()) : $the_query_nonsticky->the_post();
                            $index++;
                            if($index > 1){
                            	$cropsize = 180;
                            	$cssTitle = 'title-2';
                            	$cssText = 'text-2';
                            
                            }
                            /*if($index == 2){ ?>
                                <article id="post-special" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
                                    <div class="row">
                                        <img src="<?php bloginfo('stylesheet_directory'); ?>/images/binvinyete2_180x180_retina.jpg" alt="Charmy" />
                                    </div><!-- row-->
                                </article> <!-- Article -->
                            <?php    
                            }*/
                       ?><article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
                                  <div class="row">
                                      <?php global $post; 


                                        $textsize = 'col-md-7'; 
                                        if (has_post_thumbnail( $post->ID ) ) {
                                            $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); 
                                            $thumbnailURL = $image_url[0]; 
                                            $image = aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
                                            if(empty($image)) { $image = $thumbnailURL; } ?>
                                            <div class="col-md-5">
                                                <div class="imghoverclass img-margin-center">
                                                    <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
                                                        <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
                                                    </a> 
                                                 </div>
                                             </div>
                                            <?php $image = null; $thumbnailURL = null; 
	                                    } else {
	                                      $thumbnailURL = virtue_post_default_placeholder();
	                                      $image = aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
	                                      if(empty($image)) { $image = $thumbnailURL; } ?>
	                                      <div class="col-md-5">
	                                        <div class="imghoverclass img-margin-center">
	                                            <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
	                                                <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
	                                            </a> 
	                                         </div>
	                                     </div>
	                                    <?php $image = null; $thumbnailURL = null; 
	                                    } ?>

                                  <div class="<?php echo esc_attr($textsize);?> postcontent">
	                          		  <div class="squarre-gradient"></div>
                                      <header>
                                          <a href="<?php the_permalink() ?>"><h2 class="entry-title <?php echo $cssTitle; ?>" itemprop="name headline"><?php the_title(); ?></h2></a>
                                           <?php // get_template_part('templates/entry', 'meta-subhead'); ?>    
                                      </header>
                                    </div>

                                    <div class="<?php echo esc_attr($textsize);?> postcontent-excerpt">
                                      <header>
                                          <a href="<?php the_permalink() ?>"><h2 class="entry-title <?php echo $cssTitle; ?>" itemprop="name headline"><?php the_title(); ?></h2></a>
                                      </header>
                                      <div class="entry-content <?php echo $cssText; ?>" itemprop="articleBody">
                                          <?php 
                                          # First Article is long desc
                                          if($index == 1) {add_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );}
                                          else {
                                            add_filter( 'excerpt_length', 'custom_excerpt_length_short', 999 );
                                            remove_filter('excerpt_more', 'kadence_excerpt_more');
                                          }
                                          the_excerpt();
                                          if($index == 1) {remove_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );}
                                          else {remove_filter( 'excerpt_length', 'custom_excerpt_length_short', 999 );}
                                         ?>
                                      </div>
                                      <div class="plus-read-more"><a href="<?php the_permalink() ?>"></a></div>
                                    </div>
                              </div>
                          </article><?php 
                    endwhile;
                }
                wp_reset_postdata();

                echo "</p></div>";
            } // /foreach tagsList
            ?>
</div>
		
	<?php get_template_part('templates/page-bottom'); ?>
		
    </div><!-- /.main -->