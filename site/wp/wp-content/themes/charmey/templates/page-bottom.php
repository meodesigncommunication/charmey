         <div id="page-bottom-container">
             <article id="page-bottom-element-sejour">
                     <div id="hide_content"><?php echo $data ?></div>
                     <div class="row">
                         <div class="page-bottom-element postcontent">
                             <?php
                                 $url = 'http://static.stnet.ch/wispo/export/xml/station_4_fr.xml';
                                 $ch = curl_init();
                                 curl_setopt($ch, CURLOPT_URL, $url);
                                 curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                 $xmlresponse = curl_exec($ch);
                                 $xml = simplexml_load_string($xmlresponse);

                                 foreach ($xml->STATION->INFO as $key => $info)
                                 {

                                     if($info->attributes()->ID == 'SKI')
                                     {
                                         foreach($info as $value)
                                         {
                                             if($value->attributes()->ID == 'SKDailyNumberOfOpenFacilities')
                                             {
                                                 echo '<p id="info_installation"><img src="'.esc_url( get_stylesheet_directory_uri() ).'/images/icon_cabine.png" alt="Pistes"><span>' . $value->VALUE . '/6</span></p>';
                                             }
                                             if($value->attributes()->ID == 'SKDailySlopesOpen')
                                             {
                                                 echo '<p id="info_piste"><img src="'.esc_url( get_stylesheet_directory_uri() ).'/images/icon_ski.png" alt="Pistes"><span>' . $value->VALUE . '/30 ' . $value->UNIT_OF_MEASUREMENT . '</span></p>';
                                             }
                                         }
                                     }
                                 }
                             ?>
                         </div>
                         <div class="page-bottom-description">
                             <div id="description-bulletin" class="page-bottom-description"><a target="_blank" title="Bulletin d'enneigement" href="http://snow.myswitzerland.com/bulletin_enneigement/FribourgRegion/Charmey-4"><?php echo __('[:en]Snow report[:fr]Bulletin<br/>d\'enneigement[:de]Snow report'); ?></a></div>
                         </div>
                     </div>
             </article>
              
              <article id="page-bottom-element-meteo">
                  <div class="row">
                      <div class="page-bottom-element postcontent widget-area" role="complementary">
                      <?php // <a href="<?php echo get_the_permalink(51) ? >"><img src="< ?php echo esc_url( get_stylesheet_directory_uri() ); ? >/images/meteo.png" alt="" /></a>
                      if ( is_active_sidebar( 'home_weather_area' ) ) : ?>
								<?php dynamic_sidebar( 'home_weather_area' ); ?>
						<?php endif; ?>
						</div>
                      <div class="page-bottom-description"><?php echo __('[:en]Weather[:fr]M&eacute;t&eacute;o[:de]Wetter'); ?></div>
                 </div>
              </article>
              
              <article id="page-bottom-element-question">
                  <div class="row">
                      <div class="page-bottom-element postcontent">
                              <a href="<?php echo get_the_permalink(1033) ?>"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/question.png" alt="" /></a>
                      </div>
                      <div class="page-bottom-description"><?php echo __('[:en]Contact us[:fr]Nous contacter[:de]Kontakt'); ?></div>
                 </div>
              </article>
        </div>
        <script type="text/javascript">
            window.$ = jQuery;
            $(document).ready(function(){

                var pistes = $('#hide_content #info_stage .info_block.first #info_set1_stage .left_content .row.last .elem_left em.open').html();
                var installations = $('#hide_content #info_stage .info_block.first #info_set1_stage .left_content .row.last .elem_right em.open').html();

                $('#info_piste span').html(pistes.substr(51));
                $('#info_installation span').html(installations.substr(51));

            });
        </script>