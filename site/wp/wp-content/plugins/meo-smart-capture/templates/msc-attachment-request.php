<?php
/**
 * MSC Attachment Request
 */
$body_class = '';

$file_id = (int) $_GET['file_id'];
$post_id = (int) $_GET['post_id'];

$page = get_post($post_id);
if (!empty($page)) {
	$body_class = 'download-page-' . $page->post_name;
}
# echo get_the_title($post_id). ' - '.$post_id;
?><!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<!--[if lte IE 8]><meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=IE8" /><![endif]-->
<title><?php wp_title( '' ); // wp_title is filtered by includes/customizations.php risen_title() ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); // prints out JavaScript, CSS, etc. as needed by WordPress, theme, plugins, etc. ?>
</head>

<body <?php body_class($body_class); ?>>
    <div class="bloc-file-request">
        <h1 class="title-1 dark-brown"><?php echo __('[:en]T&eacute;l&eacute;chargement fiche[:fr]T&eacute;l&eacute;chargement fiche[:de]T&eacute;l&eacute;chargement fiche'); ?></h1>
        <h2 class="title-2 dark-brown"><?php echo __(get_the_title($post_id)); ?></h2> 
    </div>
    <div class="bloc-info">
        <img width="120" class="sub-elmt-pdf-img" src="<?php bloginfo('stylesheet_directory'); ?>/images/cahier.png">
        <div class="bloc-file-request text-2">
            <p><strong><?php echo __('[:en]Contenus de la fiche[:fr]Contenus de la fiche[:de]Contenus de la fiche') ?></strong></p>
            <p><?php echo __('[:en]plans d\'acc&egrave;s, parkings, infos compl&eacute;mentaires, bon plan, etc.[:fr]plans d\'acc&egrave;s, parkings, infos compl&eacute;mentaires, bon plan, etc.[:de]plans d\'acc&egrave;s, parkings, infos compl&eacute;mentaires, bon plan, etc.') ?></p>
        </div>
    </div>
    <div class="margintop30">
	    <?php
	    while ( have_posts() ) : the_post();
	            the_content();
	    endwhile;
		?>
	</div>
	<?php 
    wp_footer();
    ?>
</body>
</html>
