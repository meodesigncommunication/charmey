<?php
/*
 * Template Name: TPL HOME PAGE
 */
?>
<?php get_header(); ?>

	<ul class="cb-slideshow">
            <li><span>Image 01</span><div></div></li>
            <li><span>Image 02</span><div></div></li>
            <li><span>Image 03</span><div></div></li>
            <li><span>Image 04</span><div></div></li>
        </ul>
        <div class="container">
            <!-- Codrops top bar -->
            <div class="codrops-top">
                <div id="logo"><img src="wp/wp-content/themes/html5blank-stable/images/charmey-logo.png" alt="Charmey" /></div>
                <div class="clr"></div>                
            </div><!--/ Codrops top bar -->
        </div>
        <header>
                <div class="top-head">Binviny&ecirc;te</div>
				<h2>Toutes les informations touristiques sur <a target="_blank" href="http://www.la-gruyere.ch/fr/Destinations/charmey.html">www.la-gruyere.ch/fr/Destinations/charmey.html</a> ou nous contacter au <span class="no-break">+41 26 927 55 80</span></h2>
        </header>
        <div id="mailchimp-form">
            <div id="mailchimp-content">
                <?php echo do_shortcode('[mc4wp_form id="6"]'); ?>
            </div>
        </div>


	</body>
</html>