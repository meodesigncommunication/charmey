<div id="content" class="container">
  <div class="row row-archive">
        <?php if(isset($virtue['blog_archive_full']) && $virtue['blog_archive_full'] == 'full') {
            $summery    = 'full';
            $postclass  = "single-article fullpost";
          } else {
            $summery = 'normal';
            $postclass = 'postlist';
          } ?>
    <div id="--category-list" class="main <?php echo esc_attr(kadence_main_class()); ?>  <?php echo esc_attr($postclass);?> --categorylist main-archive" role="main">
    <?php
    $cat_id = get_query_var('cat');
    $imageCategory = z_taxonomy_image_url($cat_id); 
    ?>
    <?php $category_description = category_description(); /*?>
    	<div class="bloc-category-article">
           <h1 class="entry-title"><?php echo single_cat_title(); ?></h1>                
           <?php // echo $category_description; ?>
        </div>
    <?php *//* if (((isset($category_description)) && !empty($category_description)) || file_exists($imageCategory)) : ?>
        <div class="data-category">
            <div class="bloc-image-category">
                <img src="<?php echo $imageCategory; ?>" alt="<?php echo single_cat_title( '', true); ?>"/>
            </div>
            <div class="bloc-category-article">
                <h1 class="entry-title"><?php echo single_cat_title(); ?></h1>                
                <?php echo $category_description; ?>
            </div>
        </div>
        <div class="bloc-category-article">
    <?php endif; */ ?>
	<div class="bloc-category-article">
    <?php if (!have_posts()) : ?>
        <div class="no-post-exist">
            <div class="alert">
              <?php _e('Sorry, no results were found.', 'virtue'); ?>
            </div>
            <?php get_search_form(); ?>
        </div>
    <?php endif;
    
    $cropsize = 365;
    
    ?>
           
           		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
	                      <div class="row">
	                          <?php 
	                            $textsize = 'col-md-7'; 
	                            if ($imageCategory) {
	                            	$image = @aq_resize($imageCategory, $cropsize, $cropsize, true, true, true);
                                } else {
                                	$image = esc_url( get_stylesheet_directory_uri() ).'/images/motif_365x365_retina.jpg';
                                } ?>
	                                <div class="col-md-5">
	                                    <div class="imghoverclass img-margin-center">
	                                        <a href="#" title="<?php echo single_cat_title( '', true); ?>">
	                                            <img src="<?php echo $image; ?>" alt="<?php echo single_cat_title( '', true); ?>"/>
	                                        </a> 
	                                     </div>
	                                 </div>
	                                <?php $image = null; $thumbnailURL = null; ?>
	
	                      <div class="<?php echo esc_attr($textsize);?> postcontent">
	                          <div class="squarre-gradient"></div>
	                          <header>
	                              <a href="#"><h2 class="entry-title" itemprop="name headline"><?php echo single_cat_title(); ?></h2></a>
	                               <?php // get_template_part('templates/entry', 'meta-subhead'); ?>    
	                          </header>
	                          <div class="plus-read-more"></div>
	                        </div><!-- postcontent -->
	                        
	                        <div class="<?php echo esc_attr($textsize);?> postcontent-excerpt">
	                          <header>
	                              <a href="#"><h2 class="entry-title title-1" itemprop="name headline"><?php echo single_cat_title(); ?></h2></a>
	                          </header>
	                          <div class="entry-content text-1" itemprop="articleBody">
	                              <?php echo $category_description;
	                              /*
	                              # First Article is long desc
	                              	add_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
	                              	add_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
	                             
	                              	the_excerpt();
	                              	
	    							remove_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
                              		remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
                              		*/
	                              ?>
	                          </div>
	                          <div class="plus-read-more"></div>
	                        </div>
	                  </div>
	              </article>
	              
    <?php 
            $indexLoopArch = 1;
            $arrayToSortArticles = array(
            		'forfait' => array(),
            		'information' => array(),
            		'row1' => array(),
            		'row2' => array(),
            		'normal' => array()
            );
            
            while (have_posts()) : the_post(); 
            	
              $indexLoopArch++;
              if($indexLoopArch > 1){
              	$cropsize = 180;
              }
              $post = get_post(get_the_ID());
              $ID = get_the_ID();
              $link = get_the_permalink($ID);
              $title = get_the_title($ID);
              $classesArray = get_post_class();
              $classes = '';
              foreach($classesArray as $k => $class){
              	$classes.= $class.' ';
              }

              if($ID == 5946)
              {
				  $external_link = ' target="_blank" ';
				  $link = "http://www.booking.com/";
				  $title = "";
			  }else{
				  $external_link = '';
			  }
              
              $articleHTML = '<article id="post-'.$ID.'" class="'.$classes.'" itemscope="" itemtype="http://schema.org/BlogPosting">
	                      		<div class="row">';
	                      
              $textsize = 'col-md-7'; 
              if (has_post_thumbnail( $post->ID ) ) {
              	$image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
              	$thumbnailURL = $image_url[0];
              } else {
              	$thumbnailURL = virtue_post_default_placeholder();
              } 
              $image = @aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
              if(empty($image)) { $image = $thumbnailURL; } 
	                                
	                                
              $articleHTML .= '<div class="col-md-5">
                                    <div class="imghoverclass img-margin-center">
                                        <a '.$external_link.' href="'.$link.'" title="'.$title.'">
                                            <img src="'.esc_url($image).'" alt="'.$title.'" class="iconhover" style="display:block;">
                                        </a> 
                                     </div>
                                 </div>';
              $image = null; $thumbnailURL = null;
	
	          $articleHTML .= '<div class="'.esc_attr($textsize).' postcontent">
		                          <div class="squarre-gradient"></div>
		                          <header>
		                              <a '.$external_link.' href="'.$link.'"><h2 class="entry-title title-2" itemprop="name headline">'.$title.'</h2></a>
		                          </header>
		                        </div>
	                        
	                        <div class="'.esc_attr($textsize).' postcontent-excerpt">
	                          <header>
	                              <a '.$external_link.' href="'.$link.'"><h2 class="entry-title title-2" itemprop="name headline">'.$title.'</h2></a>
	                          </header>
	                          <div class="entry-content text-2" itemprop="articleBody">';
	          
	                          if(empty($external_link)){
	                              # First Article is long desc
	                              if($indexLoopArch == 1) {
	                              	add_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
	                              	add_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
	                              }
	                              else {
	                              	remove_filter('excerpt_more', 'kadence_excerpt_more');
	                              	add_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
	                              	add_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
	                              }
	                              	$articleHTML .= get_the_excerpt();
	                              	
		    						if($indexLoopArch == 1) {
		    							remove_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
	                              		remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
		    						}
	                              else {
	                              	remove_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
	                              	remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
	                              }
							  }
	            $articleHTML .= '</div>
	                          <div class="plus-read-more"><a '.$external_link.' href="'.$link.'"></a></div>
	                        </div>
	                  </div>
	              </article>';
	            
	            $field = get_field('position', get_the_ID());
	            if($field != 'forfait' && $field != 'information' && $field != 'row1' && $field != 'row2'){
	            	$field = 'normal';
	            }
	            array_push($arrayToSortArticles[$field], $articleHTML);
	            /*
              ?>
              		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
	                      <div class="row">
	                          <?php 
	                            $textsize = 'col-md-7'; 
	                            if (has_post_thumbnail( $post->ID ) ) {
	                                $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
	                                $thumbnailURL = $image_url[0];
	                                } else {
	                                	$thumbnailURL = virtue_post_default_placeholder();
	                                } 
	                                $image = @aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
	                                if(empty($image)) { $image = $thumbnailURL; } ?>
	                                <div class="col-md-5">
	                                    <div class="imghoverclass img-margin-center">
	                                        <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
	                                            <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
	                                        </a> 
	                                     </div>
	                                 </div>
	                                <?php $image = null; $thumbnailURL = null; ?>
	
	                      <div class="<?php echo esc_attr($textsize);?> postcontent">
	                          <div class="squarre-gradient"></div>
	                          <header>
	                              <a href="<?php the_permalink() ?>"><h2 class="entry-title title-2" itemprop="name headline"><?php the_title(); ?></h2></a>
	                               <?php // get_template_part('templates/entry', 'meta-subhead'); ?>    
	                          </header>
	                        </div>
	                        
	                        <div class="<?php echo esc_attr($textsize);?> postcontent-excerpt">
	                          <header>
	                              <a href="<?php the_permalink() ?>"><h2 class="entry-title title-2" itemprop="name headline"><?php the_title(); ?></h2></a>
	                          </header>
	                          <div class="entry-content text-2" itemprop="articleBody">
	                              <?php 
	                              # First Article is long desc
	                              if($indexLoopArch == 1) {
	                              	add_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
	                              	add_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
	                              }
	                              else {
	                              	remove_filter('excerpt_more', 'kadence_excerpt_more');
	                              	add_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
	                              	add_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
	                              }
	                              	the_excerpt();
		    						if($indexLoopArch == 1) {
		    							remove_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
	                              		remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
		    						}
	                              else {
	                              	remove_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
	                              	remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
	                              }
	                              ?>
	                          </div>
	                          <div class="plus-read-more"><a href="<?php the_permalink() ?>"></a></div>
	                        </div><!-- postcontent-excerpt -->
	                  </div><!-- row-->
	              </article> <!-- Article -->
              
              <?php 
              */
              # /Include code from template
             /* */
            endwhile;
            
            // Print sorted array
            foreach($arrayToSortArticles as $field => $anArrayOfArticles){
            	if(is_array($anArrayOfArticles)){
            		foreach($anArrayOfArticles as $anArticle){
            			echo $anArticle;
            		}
            	}
            }
          //Page Navigation
          if ($wp_query->max_num_pages > 1) :
            virtue_wp_pagenav();
          endif; ?>
		
    </div>
    </div><!-- /.main -->
