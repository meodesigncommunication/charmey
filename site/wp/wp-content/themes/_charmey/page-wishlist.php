<?php
/*
Template Name: Wishlist (Assign this)
*/
?>
<div id="content" class="container page-fullwidth-php">
   	<div class="row row-archive wishlist-content-bloc">
     	<div class="main col-lg-9 col-md-8 wishlist-main postlist main-archive" role="main">
            <?php get_template_part('templates/page', 'header-wishlist'); ?>
            <?php get_template_part('templates/content', 'page'); ?>
        </div><!-- /.main -->