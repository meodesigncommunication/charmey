<?php 
/*
 * Template Name: Wpfp Full Page (do not set)
 */
 ?>
 <?php get_template_part('templates/head'); ?>
<body <?php body_class(); ?> id="body">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <div id="wrapper" class="container">
        <?php 
        do_action('get_header');
        get_template_part('templates/header');

        $favorite_post_ids = wpfp_get_users_favorites();
        $imageHeart = 'heart_brown_18x21_retina.png';
        if(count($favorite_post_ids) > 0){
        	$imageHeart = 'fullheart_brown_18x21_retina.png';
        }
        ?>
        <div class="wrap contentclass" role="document">   
            <div id="top-content-bar-bg">
                    <div id="top-content-bar">
                    <div class="tcb-menu-wishlist">
                        <a href="<?php echo get_page_link(2); ?>" ><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/<?php echo $imageHeart; ?>" alt="wishlist" /></a>
                        <a id="news_nav" href="<?php echo get_page_link(5465); ?>" title="<?php echo __('[:fr]Actualit&eacute;[:en]News[:de]News'); ?>" ><?php echo __('[:fr]Actualit&eacute;[:en]News[:de]News'); ?></a>
                        <a target="_blank" id="webcam_nav" href="http://www.telecabine-charmey.ch/fr/pratique--webcams.html" title="<?php echo __('[:fr]Webcam[:en]Webcam[:de]Webcam'); ?>" ><i class="fa fa-video-camera" aria-hidden="true"></i></a>
                    </div>
                    <div class="tcb-menu-category"><?php wp_nav_menu( array( 'menu' => 'Category Menu', 'container' => 'div', 'container_class' => 'inline-menu', 'menu_class' => 'inline-menu', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>') ); ?></div>
                    <div class="tcb-menu-mainmenu">
                            <a id="news_nav" href="<?php echo get_page_link(5465); ?>" title="<?php echo __('[:fr]Actualit&eacute;[:en]News[:de]News'); ?>" ><?php echo __('[:fr]Actualit&eacute;[:en]News[:de]News'); ?></a>
                            <a target="_blank" id="webcam_nav" href="http://www.telecabine-charmey.ch/fr/pratique--webcams.html" title="<?php echo __('[:fr]Webcams[:en]Webcams[:de]Webcams'); ?>" ><?php echo __('[:fr]Webcams[:en]Webcams[:de]Webcams'); ?></a>
                            <?php /*<a id="jump-top" href="#body" class="menu-toggle show-m"></a>*/?>
                            <a id="close-down" href="#html" class="menu-toggle hide-m"></a>
                            <nav id="primary-navigation" class="main-nav" role="navigation">
                            <?php wp_nav_menu( array( 'menu' => 'Main Menu') ); ?>
                            </nav>

                    </div>
                    </div>
            </div>
            <div id="content" class="container page-fullwidth-php">
                <div class="row row-archive wishlist-content-bloc">
                    <div class="main col-lg-9 col-md-8 wishlist-main postlist main-archive" role="main">
                        <?php get_template_part('templates/page', 'header-wishlist'); ?>
                        <?php get_template_part('wpfp-page-template'); ?>
                        <?php // global $virtue; if(isset($virtue['page_comments']) && $virtue['page_comments'] == '1') { comments_template('/templates/comments.php');} ?>
                    </div><!-- /.main -->
                    <?php if (kadence_display_sidebar()) : ?>
                    <aside class="<?php echo esc_attr(kadence_sidebar_class()); ?> kad-sidebar" role="complementary">
                      <div class="sidebar">
                        <?php include kadence_sidebar_path(); ?>
                      </div><!-- /.sidebar -->
                    </aside><!-- /aside -->
                    <?php endif; ?>
                </div><!-- /.row-->
            </div><!-- /.content -->
        </div><!-- /.wrap -->
        <?php do_action('get_footer');
        get_template_part('templates/footer'); ?>
    </div><!--Wrapper-->
  </body>
</html>