<?php 
# File request exception
$fileRequestPageID = 1043;
$fileDownloadPageID = 1045;
$sharedWishlistPopupID = 1500;
# Tags list (#Couples, #Familles, #Groupes Et Entreprises
$tagsList = array(30, 29, 31);

if(is_page($fileRequestPageID) || is_page($sharedWishlistPopupID) || is_page($fileDownloadPageID) ) {
	include kadence_template_path();
}
else {
	get_template_part('templates/head'); ?>
<body <?php body_class(); ?> id="body">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <div id="wrapper" class="container">
    <?php do_action('get_header');
	    get_template_part('templates/header');
	    
	    $favorite_post_ids = wpfp_get_users_favorites();
	    $imageHeart = 'heart_brown_18x21_retina.png';
        if(count($favorite_post_ids) > 0){
        	 $imageHeart = 'fullheart_brown_18x21_retina.png';
        }
    ?>
      <div class="wrap contentclass" role="document"><div id="bg-image-shadow">
			
			<?php 
			#if not front page: add menu bars 
			if(!is_front_page()){
				?>
				<div id="top-content-bar-bg"></div>
                <nav id="primary-navigation" class="main-nav" role="navigation">
                    <div>
                        <div>
                            <h2 class="rounded"></h2>
                            <?php wp_nav_menu( array( 'menu' => 'menu_1') ); ?>
                        </div>
                        <div>
                            <h2 class="rounded"></h2>
                            <?php wp_nav_menu( array( 'menu' => 'menu_2') ); ?>
                        </div>
                        <div>
                            <h2 class="rounded"></h2>
                            <?php wp_nav_menu( array( 'menu' => 'menu_3') ); ?>
                        </div>
                        <div class="navigation_1">
                            <?php if (is_active_sidebar('navigation_1') ) { ?>
                                <div>
                                    <?php dynamic_sidebar('navigation_1'); ?>
                                </div>
                            <?php }; ?>
                        </div>
                        <div class="navigation_2">
                            <?php if (is_active_sidebar('navigation_2') ) { ?>
                                <div>
                                    <?php dynamic_sidebar('navigation_2'); ?>
                                </div>
                            <?php }; ?>
                        </div>
                    </div>
                </nav>
				<div id="top-content-bar">
					<div id="wishlist-counter"><?php echo __('[:fr]Ma liste d\'envie[:en]My wishlist[:de]My wishlist'); ?></div>
					<div class="tcb-menu-wishlist">
						<a href="<?php echo get_page_link(2); ?>" title="<?php echo __('[:fr]Ma liste d\'envie[:en]My wishlist[:de]My wishlist'); ?>" ><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/<?php echo $imageHeart; ?>" alt="wishlist" /></a>
					</div>
					<div id="topbar-search" class="topbar-widget">
			            <?php if(kadence_display_topbar_widget()) { 
			            	if(is_active_sidebar('topbarright')) { dynamic_sidebar('topbarright'); } 
			              } ?>
						<a id="news_nav" href="<?php echo get_page_link(5465); ?>" title="<?php echo __('[:fr]Actualit&eacute;[:en]News[:de]News'); ?>" ><?php echo __('[:fr]Actualit&eacute;[:en]News[:de]News'); ?></a>
						<a target="_blank" id="webcam_nav" href="http://www.telecabine-charmey.ch/fr/pratique--webcams.html" title="<?php echo __('[:fr]Webcams[:en]Webcams[:de]Webcams'); ?>" ><?php echo __('[:fr]Webcams[:en]Webcams[:de]Webcams'); ?></a>
			        </div>
					<div class="tcb-menu-category"><?php wp_nav_menu( array( 'menu' => 'Category Menu', 'container' => 'div', 'container_class' => 'inline-menu', 'menu_class' => 'inline-menu', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>') ); ?></div>
					<div class="tcb-menu-mainmenu">
						<?php /*<a id="jump-top" href="#body" class="menu-toggle show-m"></a>*/?>
						<a id="close-down" href="#html" class="menu-toggle hide-m"></a>
						<!--<nav id="primary-navigation" class="main-nav" role="navigation">
						<?php //wp_nav_menu( array( 'menu' => 'Main Menu') ); ?>
						</nav>-->
						<div class="tcb-menu-search"><a href="#" title="Recherche" onclick="showHideSearch();"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ); ?>/images/search_brown_18x21_retina.png" alt="recherche" /></a></div>
					</div>
					<?php 
					get_template_part('templates/searchform-redirect', get_post_format());
					?>
				</div>
				<?php 
			}
			include kadence_template_path(); ?>
            
          <?php if (kadence_display_sidebar()) : ?>
            <aside class="<?php echo esc_attr(kadence_sidebar_class()); ?> kad-sidebar" role="complementary">
              <div class="sidebar">
                <?php include kadence_sidebar_path(); ?>
              </div><!-- /.sidebar -->
            </aside><!-- /aside -->
          <?php endif; ?>
          </div><!-- /.row-->
        </div></div><!-- /.content -->
      </div><!-- /.wrap -->
      <?php do_action('get_footer');
      get_template_part('templates/footer'); ?>
    </div><!--Wrapper-->
  </body>
</html><?php 
}
?>