<?php
/*
Template Name: XML template
*/

$att = 'attribueName';
$url = 'http://static.stnet.ch/wispo/export/xml/station_4_fr.xml';

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$xmlresponse = curl_exec($ch);
$xml = simplexml_load_string($xmlresponse);

foreach ($xml->STATION->INFO as $key => $info)
{

    if($info->attributes()->ID == 'SKI')
    {
        foreach($info as $value)
        {
            if($value->attributes()->ID == 'SKDailyNumberOfOpenFacilities')
            {
                echo '<p>' . $value->VALUE . '/6' . '</p>';
            }
            if($value->attributes()->ID == 'SKDailySlopesOpen')
            {
                echo '<p>' . $value->VALUE . '/30 ' . $value->UNIT_OF_MEASUREMENT . '</p>';
            }
        }
    }
}