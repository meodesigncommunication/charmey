<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
                <script type="text/javascript" src="wp/wp-content/themes/html5blank-stable/js/modernizr.custom.86080.js"></script>
		<link href='http://fonts.googleapis.com/css?family=Ubuntu:400,500' rel='stylesheet' type='text/css'>
                <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
                <link href='wp/wp-content/themes/html5blank-stable/css/demo.css' rel='stylesheet' type='text/css'>
                <link href='wp/wp-content/themes/html5blank-stable/css/style1.css' rel='stylesheet' type='text/css'>
                

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
                    // conditionizr.com
                    // configure environment tests
                    conditionizr.config({
                        assets: '<?php echo get_template_directory_uri(); ?>',
                        tests: {}
                    });
                </script>

	</head>
	<body <?php body_class(); ?>>
