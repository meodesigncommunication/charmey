<div class="page-header">
    <h1 class="entry-title title-1" itemprop="name">
        <?php // echo wpfp_before_link_img() ?>
        <?php echo kadence_title(); ?>
    </h1>
    <?php global $post; 
    if(is_page()) {
        $bsub = get_post_meta( $post->ID, '_kad_subtitle', true );
        if(!empty($bsub)){
                echo '<p class="subtitle"> '.__($bsub).' </p>';
        } 
    } else if(is_category()) { 
            echo '<p class="subtitle">'.__(category_description()).' </p>';
    } ?>
</div>
<div class="wishlist-description-header text-1">
    <?php echo get_field('wishlist_text'); ?>
</div>