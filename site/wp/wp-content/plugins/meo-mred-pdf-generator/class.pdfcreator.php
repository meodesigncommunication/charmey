<?php
require_once( $plugin_root . 'fpdf/fpdf.php' );

class PDFCreator extends FPDF
{
    public $font = 'Arial';
    public $x;
    public $y;
    public $newX = 0;
    public $newY = 0;
    public $convertPxToMm = 0.264583333;
    public $pageHeight = 300;
    public $pageNum;
    
    function initPagePDF($fontWeight = 'B'){
        $this->pageNum++;
        $this->AddPage();
        $this->SetLeftMargin(5);
        $this->SetRightMargin(5);
        $this->SetFont($this->font,$fontWeight, 25);
    }
    
    function Header()
    {
        $this->SetFont('Arial','B',20);
        $this->SetDrawColor(254,212,64);
        $this->SetLineWidth(2);
        $this->SetFillColor(90,50,38);
        $this->SetXY(0,0);
        $this->SetLeftMargin(0);
        $this->SetRightMargin(0);
        $this->cell(0, 30, '', 'B', 2, 'L', true);
        $this->image(dirname(__FILE__).'/../../uploads/2015/04/logo-temp.jpg',5,5);
    }
    
    function convertStringPDFUtf8Decode($string)
    {
        if(!is_array($string))
        {
            $string = strip_tags(utf8_decode($string));        
            $string = str_replace('&nbsp;', ' ', $string);
        }
        return $string;
    }
    
    function convertStringPDFUtf8Encode($string)
    {
        $string = utf8_encode(strip_tags($string));
        $string = str_replace('&nbsp;', ' ', $string);
        return $string;
    }
    
    function generateTabFileAttachment($id, $size = 'Medium')
    {
        $thumb = wp_get_attachment_image_src($id,$size);
        $fullPath = null;
        
        if(!empty($thumb[0]))
        {
            $url = $this->convertStringPDFUtf8Encode($thumb['0']);
            $path = explode('/uploads/', $url);
            $fullPath = UPLOADS_DESTINATION.$this->convertStringPDFUtf8Decode($path[1]);
        }
        return [
            'path' => $fullPath,
            'width' => $thumb[1],
            'height' => $thumb[2]
        ];
    }

    function generateArrayImageGalleryContent($contentBase)
    {
        $content = str_replace('[gallery ids="', '*[*', $contentBase);    

        $start = (strpos($content,'*[*'));
        $end  = (strpos($content,'"]'));
        $diff = $end-$start;

        if(!empty($start) && !empty($content))
        {
            $tabImage = substr($content,($start+3),($diff-3));
            $content = substr($content,0,$start);
            
            $arrayId = explode(',',$tabImage);
            return [
                'content' => $content,
                'imageID' => $arrayId
            ];
        }else{
            return [
                'content' => $content,
                'imageID' => null
            ];
        }
    }

    
    function insertImageContent($tabImageContentParam)
    {    
        if(!empty($tabImageContentParam) && is_array($tabImageContentParam)){    
            
            $x = $this->GetX();
            $this->newX = $x;
            $count = 1;
            
            foreach($tabImageContentParam as $fileAttachmentId)
            {
                $fileAttachment = $this->generateTabFileAttachment($fileAttachmentId,[150,150]);
                // Info taille de l'image
                $widthImage = round($fileAttachment['width']*$this->convertPxToMm)+5;
                $heightImage = round($fileAttachment['height']*$this->convertPxToMm)+5;   
                if($count >= 5)
                {
                    $this->newX = $x;
                    $this->SetY($this->newY);
                    $count = 1;
                    
                    if(($this->GetY()+$heightImage) >= ($this->pageHeight*$this->pageNum))
                    {
                        $this->initPagePDF('');
                        $this->Ln(10); 
                    }
                }                
                // Gestion des position Y dans le PDF
                if($this->newY < $this->GetY()+$heightImage)
                {
                    $this->newY = $this->GetY()+$heightImage;
                }
                $this->image($fileAttachment['path'],$this->newX,$this->GetY());  
                $this->newX = $this->newX+$widthImage;                
                $count++;
            }            
        } 
    }
    
    function insertSpecialInfo($infoText){
        if(!empty($infoText)){
            $this->SetFont('Arial','B', 16);
            $this->SetTextColor(254,212,64);
            $this->cell(0,15,'Informations','TB');
            $this->Ln(15);
            $this->SetFont('Arial','', 12);
            $this->SetTextColor(0,0,0);
            $this->cell(0,10,$infoText);
        }
    }
    
}//end of class