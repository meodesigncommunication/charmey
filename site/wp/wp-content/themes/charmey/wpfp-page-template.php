<?php 
##########################
## Sharing the Wishlist ##
##########################
#
#	To share the Wishlist, we will generate during the posts loop,
#	a link containing the post ids (TODO: encrypted or not?)
#	The link will be something like domainname.com/wishlist/11-1-3-42-25
#
#	When a user land on the page with this URL, we will extract the post ids,
#	and regenerate the wishlist page (TODO: add some controls and verifications)
#
#	This way, we don't have to save the list to the Db
#	TODO (Done): Maybe develop the possibility to 'import' the presented Wishlist into the user COOKIES (if empty)
#
##########################

	$initPostIdsURL = get_permalink();

	// Var containing the HTML of the shared Wishlist
	$sharedWishlist = '';
	
	# FROM URL
	
	// We are coming from a shared wishlist url
	if(isset($_GET['wishes']) && !empty($_GET['wishes'])){
		
		// Extract items ID
		$items = explode('-', $_GET['wishes']);
		
		$nbItems = count($items);
		
		if($nbItems > 0) {
		
			$titleSharedWishlist = __('[:en]Shared Wishlist[:de]Shared Wishlist[:fr]Liste d\'envies partag&eacute;e');
			$sharedWishlist = '<div id="shared-wishlist" class="my-wishlist"><h2 class="shared-title title-1">'.$titleSharedWishlist.'</h2>
								<div class="wishlist-container">';
			
			$validItem = '';
			
			foreach($items as $item){
				
				if(is_numeric($item)){
					
					$wish = get_post($item);
                                        
					// Work only with published items
					if($wish->post_status == 'publish'){
						
						// TODO: create a sub template?
						$sharedWishlist.= '<article class="wishlist-item post type-post status-publish format-standard hentry"><div class="row">';
						
						$currentWishID= $wish->ID;
						
						$validItem.= $currentWishID.'-';
						
						/* Post Category (only one for now maybe) */
	        			$category = reset(get_the_category($currentWishID));
	        			
	        			/* Check if item has already been favorited */
	        			$action = wpfp_check_favorited($currentWishID) ? 'remove' : 'add';
	        			
					
			        	/* Post Category (only one for now maybe) */
			        	$category = reset(get_the_category($currentWishID));
			        	
			        	if (has_post_thumbnail( $currentWishID ) ) {
							$image_url = wp_get_attachment_image_src( 
							get_post_thumbnail_id( $currentWishID ), 'full' ); 
							$thumbnailURL = $image_url[0]; 
							$image = aq_resize($thumbnailURL, 150, 150, true);
							if(empty($image)) { $image = $thumbnailURL; }
                                                        
                                                        $sharedWishlist.= '<div class="wishlist-thumbnail col-md-5">';
                                                        $sharedWishlist.=   '<div class="imghoverclass img-margin-center">';
                                                        $sharedWishlist.=       '<a href="'.get_the_permalink($currentWishID).'" title="'.get_the_title($currentWishID).'">';
                                                        $sharedWishlist.=           '<img src="'.esc_url($image).'" alt="'.get_the_title().'" class="iconhover" style="display:block;">';
                                                        $sharedWishlist.=       '</a>';
                                                        $sharedWishlist.=   '</div>';
                                                        $sharedWishlist.= '</div>';
                                                                
							$image = null; 
                                                        $thumbnailURL = null; 
						}
                                                
                                                $sharedWishlist.= '<div class="col-md-7 postcontent">';
	                                            $sharedWishlist.=   '<div class="squarre-gradient"></div>';
                                                $sharedWishlist.=   '<header>';
                                                $sharedWishlist.=       '<a href="'.get_permalink($currentWishID).'" title="'. get_the_title($currentWishID) .'">';
                                                $sharedWishlist.=           '<h2 class="entry-title title-2" itemprop="name headline">'.get_the_title($currentWishID).'</h2>';
                                                $sharedWishlist.=       '</a>';
                                                $sharedWishlist.=   '</header>';
                                               // $sharedWishlist.=   '<div class="plus-read-more">';
                                               // $sharedWishlist.=       '<a href="'.get_permalink($currentWishID).'" title="'. get_the_title($currentWishID) .'">';
                                               // $sharedWishlist.=           '+';
                                               // $sharedWishlist.=       '</a>';
                                               // $sharedWishlist.=   '</div>';
                                                $sharedWishlist.= '</div>';
                                                
                                                
                                                $sharedWishlist.= '<div class="col-md-7 postcontent-excerpt">';
                                                $sharedWishlist.=   '<header>';
                                                $sharedWishlist.=       '<a href="'.get_permalink($currentWishID).'" title="'. get_the_title($currentWishID) .'">';
                                                $sharedWishlist.=           '<h2 class="entry-title title-2" itemprop="name headline">'.get_the_title($currentWishID).'</h2>';
                                                $sharedWishlist.=       '</a>';
                                                $sharedWishlist.=   '</header>';
                                                $sharedWishlist.=   '<div class="entry-content text-2" itemprop="articleBody">';
                                                $sharedWishlist.=       '<p>'.$wish->post_excerpt.'</p>';
                                                $sharedWishlist.=   '</div>';
                                                $sharedWishlist.=   '<div class="plus-read-more">';
                                                $sharedWishlist.=       '<a href="'.get_permalink($currentWishID).'" title="'. get_the_title($currentWishID) .'">';
                                                $sharedWishlist.=           '+';
                                                $sharedWishlist.=       '</a>';
                                                $sharedWishlist.=   '</div>';
                                                $sharedWishlist.= '</div>';
						
						/* Will add button only to add item to Wishlist, for now */
						if($action == 'add') $sharedWishlist.= wpfp_link(1, $action, 1, array("post_id" => $wish->ID));
						
						$sharedWishlist.= "</div></article>"; // End item
						
					}
					
					wp_reset_postdata();
					 
				} // End is_num
			} // End foreach
			
			// Link to 'import' the entire Wishlist
			$fullWishlist  = '<a class="shared-wishlist-add-shared wpfp-link-full" data-list="'.substr($validItem, 0, -1).'" href="#" title="Ajouter dans ma liste"><i class="fa fa-plus-square" aria-hidden="true"></i></a>';
			$fullWishlist .= '<div class="endCol"></div>';
			
			$sharedWishlist.= '</div>'
								.$fullWishlist.
							'</div>'; // End my wishlist
			
		} // End if $nbItems
	} // End isset
	
	// We have a shared Wishlist to present
	if($sharedWishlist) {
		echo $sharedWishlist;
	}
	
	// wpfp_update_user_meta($arr)


##########################

	# NORMAL LANDING

	global $user, $favorite_post_ids;
	
	$favorite_post_ids = wpfp_get_users_favorites();

    $wpfp_before = "";
   # echo "<div class='wpfp-span'>";
    if (isset($user) && !empty($user)) {
        if (wpfp_is_user_favlist_public($user)) {
            $wpfp_before = "$user's Favorite Posts.";
        } else {
            $wpfp_before = "$user's list is not public.";
        }
    }

    if ($wpfp_before):
       # echo '<div class="wpfp-page-before">'.$wpfp_before.'</div>';
    endif;

    /*echo '<div id="my-wishlist" class="my-wishlist"><h2>My Wishlist</h2>
    		<div class="wishlist-container">';*/
    if ($favorite_post_ids) {
		
		$favorite_post_ids = array_reverse($favorite_post_ids);
        $post_per_page = wpfp_get_option("post_per_page");
        $page = intval(get_query_var('paged'));
		
        // To present only the published ones, in case they were unpublished by the time we present the Wishlist page
        $qry = array('post__in' => $favorite_post_ids, 
        				'posts_per_page'=> $post_per_page, 
        				'orderby' => 'post__in', 
        				'paged' => $page,
        				'post_status' => 'publish',
        				'ignore_sticky_posts' => true);
        // custom post type support can easily be added with a line of code like below.
        // $qry['post_type'] = array('post','page');
        /* Sort by Category
        add_filter('posts_join', create_function('$a', 'global $wpdb; return $a . " INNER JOIN $wpdb->term_relationships ON ($wpdb->posts.ID = $wpdb->term_relationships.object_id) INNER JOIN $wpdb->term_taxonomy ON ($wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id) ";'));
		add_filter('posts_where', create_function('$a', 'global $wpdb; return $a . " AND $wpdb->term_taxonomy.taxonomy = \'category\'";'));
		add_filter('posts_orderby', create_function('$a','global $wpdb; return "$wpdb->term_taxonomy.term_id ASC";'));
         */
        $the_query_template = new WP_Query( $qry );
     	echo '<div class="wishlist-left-content">'; 
        
        if(!empty($sharedWishlist))
        {
            echo '<h2 class="shared-title title-1">Ma liste</h2>';
        }
        
        // Do we have favorited posts?
        if($the_query_template->have_posts()) {
        	
        	// We have some items
	        $initPostIdsURL.= 'wishes/';
	        $cropsize = 365;
	        $indexLoop = 0;
	        
	        while (  $the_query_template->have_posts() ) {  
	        	
	        	$indexLoop++;
	        	if($indexLoop > 1){
	        		$cropsize = 180;
	        	}
	        	
	        	echo '<article class="wishlist-item post type-post status-publish format-standard hentry">
	        	 		<div class="row">';
	        	
	        	$the_query_template->the_post();
	        	
	        	$currentWishID = get_the_ID();
	        	
	        	/* Add it to the Shared URL */
	        	$initPostIdsURL.= $currentWishID.'-';
	        	
	        	/* Post Category (only one for now maybe) */
	        	$category = reset(get_the_category());
	        	
	        	if (has_post_thumbnail( $currentWishID ) ) {
					$image_url = wp_get_attachment_image_src( 
					get_post_thumbnail_id( $currentWishID ), 'thumbnail' ); 
					$thumbnailURL = $image_url[0]; 
					$image = aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
					if(empty($image)) { $image = $thumbnailURL; } ?>
						<div class="wishlist-thumbnail col-md-5">
							<div class="imghoverclass img-margin-center">
								<a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
									<img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
								</a>
							</div>
						</div>
					<?php $image = null; $thumbnailURL = null; 
				}
	        	
				?>
						<div class="col-md-7 postcontent">
	                          <header>
	                              <a href="<?php the_permalink() ?>"><h2 class="entry-title title-2" itemprop="name headline"><?php the_title(); ?></h2></a>
	                               <?php // get_template_part('templates/entry', 'meta-subhead'); ?>    
	                          </header>
	                        </div><!-- postcontent -->
	                        
	                        <div class="col-md-7 postcontent-excerpt">
	                          <div class="squarre-gradient"></div>
	                          <header>
	                              <a href="<?php the_permalink() ?>"><h2 class="entry-title title-2" itemprop="name headline"><?php the_title(); ?></h2></a>
	                          </header>
	                          <div class="entry-content text-2" itemprop="articleBody">
	                              <?php 
	                              add_filter( 'excerpt_length', 'custom_excerpt_length_short', 999 );
	                              remove_filter('excerpt_more', 'kadence_excerpt_more');
	                              the_excerpt();
	                              remove_filter( 'excerpt_length', 'custom_excerpt_length_short', 999 );
	                              ?>
	                          </div>
	                          <div class="plus-read-more"><a href="<?php the_permalink() ?>"></a></div>
	                        </div><!-- postcontent-excerpt -->
	                        <?php /**/
	            echo "<div class=\"wishlist-item-remove\">";
	           		 wpfp_remove_favorite_link($currentWishID);
	            echo "</div>";
	            
	            echo '	</div>
	            	</article>'; // End item
	        }
        }
        echo '</div>';        
         /*echo '<div class="navigation">';
            if(function_exists('wp_pagenavi')) { wp_pagenavi(); } else { ?>
            	<div class="alignleft"><?php next_posts_link( __( '&larr; Previous Entries', 'buddypress' ) ) ?></div>
            	<div class="alignright"><?php previous_posts_link( __( 'Next Entries &rarr;', 'buddypress' ) ) ?></div>
            <?php }
        echo '</div>';*/
        
        // <a href="'.substr($initPostIdsURL, 0, -1).'">Share My list</a>
        /*
    	echo '<div id="wishlist-footer">
	    		<div class="wish-footer-text">'.__('[:en]Share My list[:de]Share My list[:fr]Partager ma liste').'
	        	<div class="wish-footer-social-element facebook">
	        		<div class="fb-share-button" data-layout="button" data-href="'.substr($initPostIdsURL, 0, -1).'"></div>
	        	</div>
	        	<div class="wish-footer-social-element twitter">
	        		<a href="https://twitter.com/share" class="twitter-share-button" data-count="none" data-url="'.substr($initPostIdsURL, 0, -1).'">Tweet</a>
					<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?\'http\':\'https\';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\'://platform.twitter.com/widgets.js\';fjs.parentNode.insertBefore(js,fjs);}}(document, \'script\', \'twitter-wjs\');</script>
				</div>
			</div>';
		*/
        echo '<div class="wishlist-right-content">'; 
    	echo   '<div class="wishlist-social-footer">
                <div class="wish-footer-text title-3 dark-brown">'.__('[:en]Share My list[:de]Share My list[:fr]Partager ma liste').'</div>
	        		<div class="virtue_social_widget clearfix">
						<a href="http://www.facebook.com/share.php?u='.substr($initPostIdsURL, 0, -1).'&title='.__('[:en]My Wishlist[:de]My Wishlist[:fr]Ma liste d\'envies').'" class="facebook_link" title="" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="Facebook"><i class="icon-facebook"></i></a>
						<a href="http://twitter.com/intent/tweet?status='.__('[:en]My Wishlist[:de]My Wishlist[:fr]Ma liste d\'envies').'+'.substr($initPostIdsURL, 0, -1).'" class="twitter_link" title="" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="Twitter"><i class="icon-twitter"></i></a>
						<a class="social-link email-shared fancybox-iframe" href="http://charmey.ch/popup-partager-wishlist/" data-toggle="tooltip" data-placement="top" data-original-title="Email"><i class="fa fa-envelope" aria-hidden="true"></i></a>
					</div>
                    <div class="clear-both"></div>
				</div>';
    	
    	/*
    	
                    <div class="wish-footer-social-element facebook">
                        <a href="http://www.facebook.com/share.php?u='.substr($initPostIdsURL, 0, -1).'&title='.__('[:en]My list[:de]My list[:fr]Ma liste').'" target="_blank" class="social-link social-facebook"><i class="icon-facebook"></i></a>
                    </div>
                    <div class="wish-footer-social-element twitter">
                        <a href="http://twitter.com/intent/tweet?status='.__('[:en]My list[:de]My list[:fr]Ma liste').'+'.substr($initPostIdsURL, 0, -1).'" target="_blank" class="social-link social-twitter"><i class="icon-twitter"></i></a>
                    </div>
                    
					<div class="wish-footer-social-element mail">
                        <a class="social-link email-shared fancybox-iframe" href="http://charmey-test-meo.meomeo.ch/popup-partager-wishlist/"><i class="fa fa-envelope" aria-hidden="true"></i></a>
                    </div>
                    
    	 */
    	echo   '<div class="wishlist-empty-footer title-3">';
		    	echo '<div class="wishlist-delete-link title-3">';
		    	echo wpfp_clear_list_link();
		    	echo "</div>";
		echo   '</div>';
    	
        echo '</div>';
    	
        // wp_reset_query();
        wp_reset_postdata();
    } else {
        $wpfp_options = wpfp_get_options();
        echo "<div class='wishlist-empty-list'>".$wpfp_options['favorites_empty']."</div>";
    }
    echo '<div class="wishlist-bloc-footer">';
   // echo "</div>"; // End container
    
    
    
    // echo "</div>"; // End my wishlist
    echo '<div class="wishlist-message-warning">';
    wpfp_cookie_warning();
    echo "</div>";
    echo "</div>";
    ?>