<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// [HTTP_X_FORWARDED_HOST] => charmey-test-meo.meomeo.ch
// if (stripos($_SERVER['HTTP_HOST'], 'charmey-test-meo.meomeo.ch') !== false) {
// 128.65.195.28 ip address infomaniak
if (stripos($_SERVER['SERVER_ADDR'], '128.65.195.28') !== false) {
	define('DB_NAME', 'rml_meomeoch6');
	define('DB_USER', 'rml_charmey-test');
	define('DB_PASSWORD', 'samestationendeducation');
	define('DB_HOST', 'rml.myd.sharedbox.com');
}
elseif ($_SERVER['SERVER_ADDR'] == "127.0.0.1") {
	define('DB_NAME', '');
	define('DB_USER', '');
	define('DB_PASSWORD', '');
	define('DB_HOST', 'localhost');
}
elseif ($_SERVER['SERVER_ADDR'] == "192.168.67.74") {
	define('DB_NAME', '');
	define('DB_USER', '');
	define('DB_PASSWORD', '');
	define('DB_HOST', 'localhost');
}
/* Landing page
else {
	define('DB_NAME', 'mgqf_charmey');
	define('DB_USER', 'mgqf_charmeyusr');
	define('DB_PASSWORD', 'ChArM3y!SQL');
	define('DB_HOST', 'mgqf.myd.infomaniak.com');
	define('IS_PRODUCTION', true);
} */
else {
	define('DB_NAME', 'mgqf_charmeynew');
	define('DB_USER', 'mgqf_charmeyusr');
	define('DB_PASSWORD', 'ChArM3y!SQL');
	define('DB_HOST', 'mgqf.myd.infomaniak.com');
	define('IS_PRODUCTION', true);
}

if (!defined('IS_PRODUCTION')) {
	define('IS_PRODUCTION', false);
}

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1 {Ca.X;iD-uDjz|;%H0%^Tkd0LWBN0c^}-r*~jDMKAYid!0[>Lm%0bHI1|7sqKF');
define('SECURE_AUTH_KEY',  'zQ @f/rl`MlhhGHM?<_+b=-@JZ]tGE0ED/iv~r49xu~r%oVCE/t/+*uH}eYkSWQs');
define('LOGGED_IN_KEY',    ' 4)[1KIXt)BnLRXgdwVs0zhdAJ|%KpI-UI^DfK%ESM*|,9EX2uuF|.)X<k/aQy^G');
define('NONCE_KEY',        'TFbrG|[@B;TG}Yo>eCN#B([jS9P~>XlGRN%(?fE {(=Z+,-k`3}%RbB{kmC4saq5');
define('AUTH_SALT',        'D$/&@)E9z6kjv*K`(WliixW~2`A2Duv^w)u#.cv-HDx5&8&?}WxQ+]2`$cT TW0v');
define('SECURE_AUTH_SALT', '+M|B]j x&oXcE {BIK`y_9/ M.U&G^QI;~TIz%+GDz}P&7<mjBq&vcT8n,J.L+HZ');
define('LOGGED_IN_SALT',   '|l4Q*E!12Jv[.2A-K[dz6]Zb9R9[dN_4g@{[N]oV9/5dj%X>lqrM9MSP+KAQqr([');
define('NONCE_SALT',       't,[ZH:P?E(*[I;2(Mv%}!-w{ra:{!?nOB]BEr V8pP7=.(19Fza.;ZhA$F(kM,h&'); 
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false);

define( 'WP_POST_REVISIONS', 5 );

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');