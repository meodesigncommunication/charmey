<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html id="html" class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <?php global $virtue; ?>
  <title><?php wp_title( '|', true, 'right' ); ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/assets/css/swiper.min.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/slider/css/slick.css" type="text/css"/>
  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/slider/css/slick-theme.css" type="text/css"/>
  <?php if (!empty($virtue['virtue_custom_favicon']['url'])) {?>
  	<link rel="shortcut icon" type="image/x-icon" href="<?php echo esc_url($virtue['virtue_custom_favicon']['url']); ?>" />
  	<?php } ?>
  <?php wp_head();
  if(is_single() && !is_page()) {
  	if(has_post_thumbnail()){
	  	$thumb = get_post_thumbnail_id();
		$img_url = wp_get_attachment_url( $thumb,'large' );
		$image = aq_resize( $img_url, 380, 380, true ); //resize & crop the image
		if(empty($image)) { $image = $img_url; }
		if($image) { ?>
		<meta property="og:image" content="<?php echo esc_url($image); ?>" />
		<?php }
  	} ?>
  	<meta property="og:title" content="<?php the_title(); ?>" />
  	<meta property="og:url" content="<?php the_permalink(); ?>" />
  <?php } 
  
  if(is_home){
  ?><meta name="description" content="Site officiel de Charmey Tourisme : Toutes les informations sur les activit&eacute;s hiver &amp; &eacute;t&eacute; : cr&eacute;e ton s&eacute;jour !" /><?php 
  }?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Catamaran" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Droid+Serif" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/print.css" type="text/css" media="print" />
	<link rel="stylesheet" id="roots_child-css2" href="http://charmey.ch/wp/wp-content/themes/charmey/style.winter.css" type="text/css" media="all">
        
	<meta property="og:title" content="Charmey Tourisme" />
	<meta property="og:description" content="Site officiel de Charmey Tourisme : Toutes les informations sur les activit&eacute;s hiver &amp; &eacute;t&eacute; : cr&eacute;e ton s&eacute;jour !" />
	<meta property="og:image" content="http://charmey.ch/charmey_tourisme.jpg" />
</head>
