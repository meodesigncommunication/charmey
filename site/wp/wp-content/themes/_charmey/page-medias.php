<?php
/*
Template Name: Espace Medias
*/

global $post;

require_once get_stylesheet_directory().'/class/Mobile_Detect.php';

$detect = new Mobile_Detect();

?>

<div id="content" class="container page-php">
    <div class="simple-page-content">
        <article <?php post_class(); ?>>
            <div class="single-article-left">
                <div class="imghoverclass postfeat post-single-img" itemprop="image">
                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); ?>" alt="">
                </div>
                <header>
                    <h1 class="entry-title title-1 dark-brown" itemprop="name headline"><?php echo _e($post->post_title) ?></h1>
                </header>
                <div class="entry-content content-single-article" itemprop="description articleBody">
                    <?php echo _e($post->post_content) ?>
                </div>
            </div>

            <div class="single-article-right">
                <?php
                // Practical Information
                $havePracticalInfo = false;
                $infoText = reset(get_post_meta($post->ID, 'infos_texte'));
                $infoPDF = get_field('infos_pdf', $post->ID);
                $infoPlanStay = get_field('organiser_sejour', $post->ID);

                if($infoText != '' || $infoPDF != '' || $infoPlanStay != ''){
                    $havePracticalInfo = true;
                }

                if($havePracticalInfo){
                    ?>
                    <div class="r-info">
                        <span class="sub-elmt-title title-3 dark-brown"><?php echo __('[:fr]INFOS PRATIQUES[:en]PRACTICAL INFORMATION[:de]PRACTICAL INFORMATION'); ?></span>
                        <?php
                        if($infoText != ''){
                            ?>
                            <div class="sub-elmt-text text-2 bright-brown"><?php echo nl2br(__($infoText)); ?></div>
                            <?php
                        }
                        if($infoPDF != '' && is_array($infoPDF)){
                            ?>
                            <div class="sub-elmt-pdf">
                                <img width="90" class="sub-elmt-pdf-img" src="<?php bloginfo('stylesheet_directory'); ?>/images/IC_notebook.png">
                                <div class="sub-elmt-pdf-bg">
                                    <?php
                                    if($detect->isMobile() && !$detect->isTablet())
                                    {
                                        echo do_shortcode('[call2actionbtn title="t&eacute;l&eacute;charger le logo" url="'. get_bloginfo('home') .'/file-request/?file_id='.(($infoPDF['ID']*17)+3).'&post_id=' .  $post->ID .'&current_url='. get_bloginfo('home') .'/'. get_page_uri( $post->ID ) .'" target="_blank" linkclass="title-3 dark-brown"]');
                                    }else{
                                        echo do_shortcode('[call2actionbtn title="t&eacute;l&eacute;charger le logo" url="'. get_bloginfo('home') .'/file-request/?file_id='.(($infoPDF['ID']*17)+3).'&post_id=' .  $post->ID .'&current_url=" target="" linkclass="fancybox-iframe title-3 dark-brown"]');
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <?php
                }
                ?>
            </div>
            <footer class="single-footer"> </footer>
        </article>
    </div>
