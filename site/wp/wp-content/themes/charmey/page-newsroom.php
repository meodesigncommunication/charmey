<?php
/*
Template Name: Newsroom
*/

global $post;
$count = 0;
$order = 1;
$post_per_page = 3;
$currentLang = qtrans_getLanguage();

$args = array(
    'post_type'   => 'post_news'
);
$list_news = get_posts( $args );

/*--------------------- INSTAGRAM ---------------------*/

$client_id = '4b3bc5b8d58e466f9987a51ccec916a4';
$token_auth = '330282293.1677ed0.faf5cc3ee9644ce9b523ec1336f60555';
$countMedia = 1;

$url = "https://api.instagram.com/v1/users/self/media/recent?client_id=".$client_id."&access_token=".$token_auth."&count=".$countMedia;

try {
    // Get lasted media with a CURL request
    $curl_connection = curl_init($url);
    curl_setopt($curl_connection, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($curl_connection, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl_connection, CURLOPT_SSL_VERIFYPEER, false);

    //Data are stored in $data
    $data = json_decode(curl_exec($curl_connection), true);
    curl_close($curl_connection);

    $image_caption = $data['data'][0]['caption']['text'];
    $image_link = $data['data'][0]['images']['standard_resolution']['url'];

} catch(Exception $e) {
    return $e->getMessage();
}

/*--------------------- / INSTAGRAM ---------------------*/

?>
<div id="content" class="container page-portfolio-php">
    <div id="content-newsroom-page" class="simple-page-content">
        <div class="social-content-newsroom single-article-right">
            <section id="social-network-button">
                <?php if ( is_active_sidebar( 'widget_newsroom' ) ) : ?>
                    <?php dynamic_sidebar( 'widget_newsroom' ); ?>
                <?php endif; ?>
            </section>
            <section id="youtube-content">
                <span class="sub-elmt-title title-3 dark-brown">YOUTUBE</span>
                <iframe id="playerYoutube" style="max-width: 100%; width: 100%;" src="" frameborder="0" allowfullscreen></iframe>
            </section>
            <section id="instagram-content">
                <span class="sub-elmt-title title-3 dark-brown">INSTAGRAM</span>
                <a target="_blank" href="https://www.instagram.com/charmey_tourisme/" title="Instagram Charmey Tourisme">
                    <img src="<?php echo $image_link ?>" alt="<?php echo $image_caption ?>" />
                </a>
            </section>
            <span class="twitter-title sub-elmt-title title-3 dark-brown">TWITTER</span>
            <section id="twitter-content">
                <a class="twitter-timeline" href="https://twitter.com/charmeygruyere">Tweets by charmeygruyere</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
            </section>
            <section id="facebook-follow-me">
                <a target="_blank" class="facebook_follow_me" href="https://www.facebook.com/charmeytourisme/"><img src="<?php echo esc_url( get_stylesheet_directory_uri() ) ?>/images/facebook_follow.jpg" alt="Suiver nous sur facebook" /></a>
            </section>
        </div>
        <div class="single-article-left page-content">
            <div id="newsroom-header">
                <img src="http://charmey.ch/wp/wp-content/uploads/2017/07/rando_activity_1000x10001.jpg" alt="" />
                <div class="newsroom-content">
                    <?php echo _e($post->post_content) ?>
                </div>
            </div>
            <div class="page-header">
                <h1 class="entry-title title-1"><?php echo _e($post->post_title) ?></h1>
            </div>
            <div data-order="<?php echo $order ?>" class="all-news-content news-content-<?php echo $order ?>">
                <?php foreach($list_news as $news): ?>
                    <?php
                    $count++;
                    if($count >= $post_per_page){
                        $order++;
                        echo '</div>';
                        echo '<div data-order="'.$order.'" class="all-news-content news-content-'.$order.'" style="display:none">';
                        $count = 1;
                    }
                    ?>
                    <div class="post-news-preview">
                        <div class="news-featured">
                            <?php if ( has_post_thumbnail( $news->ID ) ): ?>
                                <a target="_blank" href="<?php echo substr(get_site_url(),0,-2).'single-news?id='.$news->ID ?>" title="<?php echo get_permalink( $news->post_title ) ?>">
                                    <?php echo get_the_post_thumbnail( $news->ID, 'full' ); ?>
                                </a>
                            <?php endif ?>
                        </div>
                        <div class="page-header">
                            <a target="_blank" href="<?php echo substr(get_site_url(),0,-2).'single-news?id='.$news->ID ?>" title="<?php echo get_permalink( $news->post_title ) ?>">
                                <h1 class="entry-title title-1"><?php echo _e($news->post_title); ?></h1>
                            </a>
                        </div>
                        <div class="news-content">
                            <?php
                            $content = qtrans_use($currentLang, $news->post_content, false);
                            echo substr($content, 0, 200).'&nbsp;&nbsp;<a target="_blank" href="' . substr(get_site_url(),0,-2).'single-news?id='.$news->ID . '" title="' . $news->post_title . '"><strong>[ lire plus ]</strong></a>';
                            ?>
                        </div>
                    </div>
                    <hr/>
                <?php endforeach; ?>
            </div>
            <div id="pager-news">
                <?php
                if($order > 1){
                    echo '<ul>';

                    for($i=1;$i<=$order;$i++){
                        $class = ($i == 1)? 'actif' : '';
                        echo '<li data-order="'.$i.'" class="'.$class.'">' . $i . '</li>';
                    }

                    echo '</ul>';
                }
                ?>
            </div>
        </div>
    </div>
    <div id="content-newsroom-page-mobile" class="simple-page-content">
        <section id="social-network-button">
            <?php if ( is_active_sidebar( 'widget_newsroom' ) ) : ?>
                <?php dynamic_sidebar( 'widget_newsroom' ); ?>
            <?php endif; ?>
        </section>
        <div id="newsroom-header">
            <img src="http://charmey.ch/wp/wp-content/uploads/2017/02/banner_newsroom_610x610.png" alt="" />
            <div class="newsroom-content">
                <?php echo _e($post->post_content) ?>
            </div>
        </div>
        <div class="page-header">
            <h1 class="entry-title title-1"><?php echo _e($post->post_title) ?></h1>
        </div>
        <div data-order="<?php echo $order ?>" class="all-news-content news-content-<?php echo $order ?>">
            <?php foreach($list_news as $news): ?>
                <div class="post-news-preview">
                    <div class="news-featured">
                        <?php if ( has_post_thumbnail( $news->ID ) ): ?>
                            <a target="_blank" href="<?php echo substr(get_site_url(),0,-2).'single-news?id='.$news->ID ?>" title="<?php echo get_permalink( $news->post_title ) ?>">
                                <?php echo get_the_post_thumbnail( $news->ID, 'full' ); ?>
                            </a>
                        <?php endif ?>
                    </div>
                    <div class="page-header">
                        <a target="_blank" href="<?php echo substr(get_site_url(),0,-2).'single-news?id='.$news->ID ?>" title="<?php echo get_permalink( $news->post_title ) ?>">
                            <h1 class="entry-title title-1"><?php echo _e($news->post_title); ?></h1>
                        </a>
                    </div>
                    <div class="news-content">
                        <?php
                        $content = qtrans_use($currentLang, $news->post_content, false);
                        echo substr($content, 0, 160).'&nbsp;&nbsp;<a target="_blank" href="' . substr(get_site_url(),0,-2).'single-news?id='.$news->ID . '" title="' . $news->post_title . '"><strong>[ lire plus ]</strong></a>';
                        ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    window.$ = jQuery;
    $(document).ready(function(){

        /*--------------------- YOUTUBE ---------------------*/
        var urlYoutube = 'https://www.youtube.com/embed/';
        var url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=UC-9al5SIrLn3m8ysPose6ZA&maxResults=1&order=date&type=video&key=AIzaSyCWpdwGRFLjjlSVAzvq1cr6p4wUVO4ZSnU';

        $.getJSON(url,
            function(response){
                var videoId = response['items'][0]['id']['videoId'];
                $('#playerYoutube').attr('src', urlYoutube + videoId);
            }
        );
        /*--------------------- / YOUTUBE ---------------------*/

        $('#pager-news ul li').click(function(){
            var order = $(this).attr('data-order');
            //$(window).scrollTop(300);
            $('html, body').animate({
                scrollTop: 300
            }, 'slow');

            $('#pager-news ul li').each(function(){
                $(this).removeClass('actif');
            });
            $('.all-news-content').each(function(){
                $(this).hide();
            });
            $('.news-content-'+order).show();
            $(this).addClass('actif');
        });
    });
</script>