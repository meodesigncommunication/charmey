<?php 
 
require_once get_stylesheet_directory().'/class/Mobile_Detect.php';

$detect = new Mobile_Detect();

if(kadence_display_sidebar()) {
        $slide_sidebar = 848;
      } else {
        $slide_sidebar = 1140;
      }
      global $post, $virtue;
      $headcontent = get_post_meta( $post->ID, '_kad_blog_head', true );
      $height      = get_post_meta( $post->ID, '_kad_posthead_height', true );
      $swidth      = get_post_meta( $post->ID, '_kad_posthead_width', true );
      if(empty($headcontent) || $headcontent == 'default') {
          if(!empty($virtue['post_head_default'])) {
              $headcontent = $virtue['post_head_default'];
          } else {
              $headcontent = 'none';
          }
      }
      if (!empty($height)) {
        $slideheight = $height; 
      } else {
        $slideheight = 400;
      }
      if (!empty($swidth)) {
        $slidewidth = $swidth; 
      } else {
        $slidewidth = $slide_sidebar;
      } ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  <div id="content" class="container">
    <div class="row single-article" itemscope="" itemtype="http://schema.org/BlogPosting">
      <div class="main <?php echo esc_attr( kadence_main_class() ); ?>" role="main">
        <?php while (have_posts()) : the_post(); ?>
          <article <?php post_class(); ?>>
           <div class="single-article-left">
            <?php if ($headcontent == 'flex') { ?>
              <section class="postfeat">
                <div class="flexslider loading kt-flexslider" style="max-width:<?php echo esc_attr($slidewidth);?>px;" data-flex-speed="7000" data-flex-anim-speed="400" data-flex-animation="fade" data-flex-auto="true">
                  <ul class="slides">
                  <?php $image_gallery = get_post_meta( $post->ID, '_kad_image_gallery', true );
                          if(!empty($image_gallery)) {
                            $attachments = array_filter( explode( ',', $image_gallery ) );
                              if ($attachments) {
                              foreach ($attachments as $attachment) {
                                $attachment_url = wp_get_attachment_url($attachment , 'full');
                                $image = aq_resize($attachment_url, $slidewidth, $slideheight, true);
                                  if(empty($image)) {$image = $attachment_url;}
                                echo '<li><img src="'.esc_url($image).'"/></li>';
                              }
                            }
                          } else {
                            $attach_args = array('order'=> 'ASC','post_type'=> 'attachment','post_parent'=> $post->ID,'post_mime_type' => 'image','post_status'=> null,'orderby'=> 'menu_order','numberposts'=> -1);
                            $attachments = get_posts($attach_args);
                              if ($attachments) {
                                foreach ($attachments as $attachment) {
                                  $attachment_url = wp_get_attachment_url($attachment->ID , 'full');
                                  $image = aq_resize($attachment_url, $slidewidth, $slideheight, true);
                                    if(empty($image)) {$image = $attachment_url;}
                                  echo '<li><img src="'.esc_url($image).'"/></li>';
                                }
                              } 
                          } ?>                            
                  </ul>
                </div> <!--Flex Slides-->
              </section>
        <?php } else if ($headcontent == 'video') { ?>
          <section class="postfeat">
            <div class="videofit">
                <?php echo get_post_meta( $post->ID, '_kad_post_video', true ); ?>
            </div>
          </section>
        <?php } else if ($headcontent == 'image') {          
                    $thumb = get_post_thumbnail_id();
                    $img_url = wp_get_attachment_url( $thumb,'large' );
                    $image = aq_resize( $img_url, 380, 380, true ); //resize & crop the image
                    if(empty($image)) { $image = $img_url; }
                    if($image) : ?>
                      <div class="imghoverclass postfeat post-single-img" itemprop="image">
                          <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" />
                      </div>
                    <?php endif; ?>
        <?php } ?>
    <?php // get_template_part('templates/post', 'date'); ?> 
    <header>
      <h1 class="entry-title title-1 dark-brown" itemprop="name headline"><?php the_title(); ?></h1>
      <?php # get_template_part('templates/entry', 'meta-subhead'); ?>  
    </header>
    <div class="entry-content content-single-article" itemprop="description articleBody">
      <?php the_content(); ?>
    </div>
    </div>
    <div class="single-article-right">
    	<div class="r-wishlist">
    	<?php 
    	// Wishlist
    	if (function_exists('wpfp_link')) { wpfp_link(); }
    	?>
    	</div>
    	
    	<?php 
    	// Practical Information
    	$havePracticalInfo = false;
    	$infoText = reset(get_post_meta($post->ID, 'infos_texte'));
    	$infoPDF = get_field('infos_pdf', $post->ID);
    	$infoPlanStay = get_field('organiser_sejour', $post->ID);
    	
    	if($infoText != '' || $infoPDF != '' || $infoPlanStay != ''){
    		$havePracticalInfo = true;
    	}
    	
    	
    	if($havePracticalInfo){
    	?>
	    	<div class="r-info">
	    		<span class="sub-elmt-title title-3 dark-brown"><?php echo __('[:fr]INFOS PRATIQUES[:en]PRACTICAL INFORMATION[:de]PRACTICAL INFORMATION'); ?></span>
	    		<?php 
		    	if($infoText != ''){
		    		?>
		    		<div class="sub-elmt-text text-2 bright-brown"><?php echo nl2br(__($infoText)); ?></div>
		    		<?php 
		    	}
		    	if($infoPDF != '' && is_array($infoPDF)){
		    		?>
		    		<div class="sub-elmt-pdf">
                         <img width="90" class="sub-elmt-pdf-img" src="<?php bloginfo('stylesheet_directory'); ?>/images/IC_notebook.png">
		    			<div class="sub-elmt-pdf-bg">
                                            <?php  
                                            if($detect->isMobile() && !$detect->isTablet())
                                            {
                                                echo do_shortcode('[call2actionbtn title="t&eacute;l&eacute;charger la fiche infos en pdf" url="'. get_bloginfo('home') .'/file-request/?file_id='.(($infoPDF['ID']*17)+3).'&post_id=' .  $post->ID .'&current_url='. get_bloginfo('home') .'/'. get_page_uri( $post->ID ) .'" target="_blank" linkclass="title-3 dark-brown"]'); 
                                            }else{
                                                echo do_shortcode('[call2actionbtn title="t&eacute;l&eacute;charger la fiche infos en pdf" url="'. get_bloginfo('home') .'/file-request/?file_id='.(($infoPDF['ID']*17)+3).'&post_id=' .  $post->ID .'&current_url=" target="" linkclass="fancybox-iframe title-3 dark-brown"]'); 
                                            }
                                            ?>
		    			</div>
		    		</div>
		    		<?php 
		    	}
		    	if($infoPlanStay != ''){
		    		?>
		    		<div class="sub-elmt-plan">
                        <img class="sub-elmt-plan-img" src="<?php bloginfo('stylesheet_directory'); ?>/images/coucou.png">
		    			<div class="sub-elmt-plan-bg">
		    				<a href="<?php echo '#'; ?>" target="_blank">
                                                    <?php echo __('[:fr]organiser votre s&eacute;jour[:en]plan your stay[:de]plan your stay'); ?>
                                                </a>
		    			</div>
		    		</div>
		    		<?php 
		    	}
		    	?>
	    	</div>
	    <?php 
    	}
    	?>
    	<?php 
    	// Gallery
    	$haveGallery = false;
    	$imgGalleryCode = '';
    	for($i=1; $i<=6; $i++){
    		$IMGgalleryID = reset(get_post_meta($post->ID, 'image_gallery_'.$i));
    		if( !empty( $IMGgalleryID ) ){
    			$haveGallery = true;
    			$IMG = reset(wp_get_attachment_image_src($IMGgalleryID));
    			$IMGhref = reset(wp_get_attachment_image_src($IMGgalleryID, 'large'));
    			$imgGalleryCode.= '<a href="'.$IMGhref.'" rel="img-gal" class="fancybox" id="a-img'.$i.'"><img src="'.$IMG.'" alt="" class="" /></a>';
    		}
    	}
    	if($haveGallery){
    		
    		echo '<div class="r-image-gallery">
    				<span class="sub-elmt-title title-3">IMAGES</span>'.$imgGalleryCode.'</div>
        			<script>jQuery(document).ready(function() {jQuery(".r-image-gallery a.fancybox").fancybox();});</script>';
    	}
    	?>
    	<div class="r-share">
    		<span class="sub-elmt-title title-3 dark-brown"><?php echo __('[:en]SHARE[:fr]PARTAGER[:de]SHARE'); ?></span>
    		<div class="virtue_social_widget clearfix">
				<a href="http://www.facebook.com/share.php?u=<?php echo urlencode(get_the_permalink()); ?>&title=<?php the_title();?>" class="facebook_link" title="" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="Facebook"><i class="icon-facebook"></i></a>
				<a href="http://twitter.com/intent/tweet?status=<?php the_title();?>+<?php echo urlencode(get_the_permalink()); ?>" class="twitter_link" title="" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="Twitter"><i class="icon-twitter"></i></a>
			</div>
    		<?php /* 
    		<div class="custom-share">
    			<a href="http://www.facebook.com/share.php?u=<?php echo urlencode(get_the_permalink()); ?>&title=<?php the_title();?>" target="_blank" class="social-link social-facebook"><i class="icon-facebook-sign"></i></a>
    			<a href="http://twitter.com/intent/tweet?status=<?php the_title();?>+<?php echo urlencode(get_the_permalink()); ?>" target="_blank" class="social-link social-twitter"><i class="icon-twitter"></i></a>
    		</div>
    		<div class="fb-share-button" data-layout="button"></div>
    		<a href="https://twitter.com/share" class="twitter-share-button" data-count="none">Tweet</a>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
			 */?>
    	</div>
    </div>
    <footer class="single-footer">
      <?php 
      /*$tags = get_the_tags(); if ($tags) { ?>
        <span class="posttags"><i class="icon-tag"></i> <?php the_tags('', ', ', ''); ?> </span>
      <?php } ?>
      <?php $authorbox = get_post_meta( $post->ID, '_kad_blog_author', true );
      if(empty($authorbox) || $authorbox == 'default') {
          if(isset($virtue['post_author_default']) && ($virtue['post_author_default'] == 'yes')) {
            virtue_author_box(); 
          }
      } else if($authorbox == 'yes'){ 
        virtue_author_box(); 
      }?>
      <?php $blog_carousel_recent = get_post_meta( $post->ID, '_kad_blog_carousel_similar', true ); 
      if(empty($blog_carousel_recent) || $blog_carousel_recent == 'default' ) { if(isset($virtue['post_carousel_default'])) {$blog_carousel_recent = $virtue['post_carousel_default']; } }
      if ($blog_carousel_recent == 'similar') { 
        get_template_part('templates/similarblog', 'carousel');
      } else if($blog_carousel_recent == 'recent') {
        get_template_part('templates/recentblog', 'carousel');
      } ?>

      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'virtue'), 'after' => '</p></nav>')); ?>
      <?php if(isset($virtue['show_postlinks']) &&  $virtue['show_postlinks'] == 1) {get_template_part('templates/entry', 'post-links'); }
      */ ?>
    </footer>
    <!-- version mobile -->
    <div class="single-article-mobile">
        <div class="single-mobile-gallery">
            <!-- boucle début -->
            <?php 
            // Gallery
            $haveGallery = false;
            $imgGalleryCode = '';
            $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
            for($i=1; $i<=6; $i++){
                    $IMGgalleryID = reset(get_post_meta($post->ID, 'image_gallery_'.$i));
                    if( !empty( $IMGgalleryID ) ){
                            $haveGallery = true;
                            $IMG = reset(wp_get_attachment_image_src($IMGgalleryID));
                            $IMGhref = reset(wp_get_attachment_image_src($IMGgalleryID, 'large'));
                            if($i == 1){
                                $imgGalleryCode.= '<div class="swiper-slide"><img src="'.$feat_image.'" alt="" class="" /></div>';
                            }
                            $imgGalleryCode.= '<div class="swiper-slide"><img src="'.$IMGhref.'" alt="" class="" /></div>';
                    }
            }
            if($haveGallery){?>
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                <?php
                echo $imgGalleryCode;
                ?>
                    </div>
                    <!-- Add Arrows -->
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>       
                <?php
            }  else {
                echo '<div class="post-single-img"><img src="'.$feat_image.'" alt="" class="" /></div>';
            }
            ?>
            <!-- boucle fin -->
        </div>
        <div class="single-mobile-wishlist">
            <?php 
                // Wishlist
                if (function_exists('wpfp_link')) { wpfp_link(); }
            ?>        
        </div>
        <div class="single-mobile-content">
            <header>
                <h1 class="entry-title title-1 dark-brown" itemprop="name headline"><?php the_title(); ?></h1>
                <?php # get_template_part('templates/entry', 'meta-subhead'); ?>  
            </header>
            <div class="entry-content content-single-article text-1" itemprop="description articleBody">
                <?php the_content(); ?>
            </div>
        </div>
        <div class="single-mobile-datas">
            <div class="single-mobile-info-pratique">
                <?php 
                    if($infoText != ''){
                            ?>
                            <span class="sub-elmt-title title-3 dark-brown"><?php echo __('[:fr]INFOS PRATIQUES[:en]PRACTICAL INFORMATION[:de]PRACTICAL INFORMATION'); ?></span>
                            <div class="sub-elmt-text text-2 bright-brown"><?php echo __($infoText); ?></div>
                            <?php 
                    }?>
                <div class="single-mobile-share">
                    <span class="sub-elmt-title title-3 dark-brown"><?php echo __('[:en]SHARE[:fr]PARTAGER[:de]SHARE'); ?></span>
                    <div class="virtue_social_widget clearfix mobile">
						<a href="http://www.facebook.com/share.php?u=<?php echo urlencode(get_the_permalink()); ?>&title=<?php the_title();?>" class="facebook_link" title="" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="Facebook"><i class="icon-facebook"></i></a>
						<a href="http://twitter.com/intent/tweet?status=<?php the_title();?>+<?php echo urlencode(get_the_permalink()); ?>" class="twitter_link" title="" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="Twitter"><i class="icon-twitter"></i></a>
					</div>
                    <div class="clear-both"></div>
                </div>
                    <?php if($infoPDF != '' && is_array($infoPDF)){
                            ?>
                            <div class="sub-elmt-pdf">
                                    <div class="sub-elmt-pdf-bg">
                                        <?php  
                                        if($detect->isMobile() && !$detect->isTablet())
                                        {
                                            echo do_shortcode('[call2actionbtn title="t&eacute;l&eacute;charger la fiche infos en pdf" url="'. get_bloginfo('home') .'/file-request/?file_id='.(($infoPDF['ID']*17)+3).'&post_id=' .  $post->ID .'&current_url='. get_bloginfo('home') .'/'. get_page_uri( $post->ID ) .'" target="_blank" linkclass="title-3 dark-brown"]'); 
                                        }else{
                                            echo do_shortcode('[call2actionbtn title="t&eacute;l&eacute;charger la fiche infos en pdf" url="'. get_bloginfo('home') .'/file-request/?file_id='.(($infoPDF['ID']*17)+3).'&post_id=' .  $post->ID .'&current_url=" target="" linkclass="fancybox-iframe title-3 dark-brown"]'); 
                                        }
                                        ?>
                                    </div>
                            </div>
                            <?php 
                    }
                    if($infoPlanStay != ''){
                            ?>
                            <div class="sub-elmt-plan">
                                    <div class="sub-elmt-plan-bg">
                                            <a href="<?php echo '#'; ?>" target="_blank">
                                                <?php echo __('[:fr]organiser votre s&eacute;jour[:en]plan your stay[:de]plan your stay'); ?>
                                            </a>
                                    </div>
                            </div>
                            <?php 
                    }
                ?>
            </div>
        </div>
        <hr/>
    </div>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
</div>
<script src="<?php echo get_stylesheet_directory_uri() ?>/assets/js/swiper.min.js"></script>
<script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 30
    });
</script>
