<?php 
    require_once 'wp/wp-content/themes/charmey/class/Mobile_Detect.php';
    $detect = new Mobile_Detect;
    
    // vars
    $cat_id = get_query_var('cat');
    
    // load thumbnail for this taxonomy term (term string)
    $linkCat = get_field('external-link', 'category_'.$cat_id);
    
    $cat_id = get_query_var('cat');
    query_posts('cat='.$cat_id.'&posts_per_page=100');
    $category_description = category_description();
    $category_title = single_cat_title("", false);
    if( $detect->isMobile() && !$detect->isTablet() ){ ?>
    
    <div id="content" class="container">
        <div class="row row-archive">
            <div class="main col-lg-9 col-md-8 postlist main-archive" role="main">  
                <article id="detail-cat">
                    <h2><?php echo $category_title ?></h2>
                    <?php 
                        if(isset($category_description) && !empty($category_description))
                        {
                            echo $category_description;
                        }                    
                    ?>
                    <a id="cat-link" target="_blank" href="<?php echo $linkCat ?>" title="<?php echo __('[:fr]Lien externe[:en]External Link') ?>">
                        <?php echo __('[:fr]Lien[:en]Link'); ?>
                    </a>
                </article>
                <div id="slick-mobile">
                    <?php                 
                    $indexLoopArch = 0;
                    $cropsize = 365;
                    
                    while (have_posts()) : the_post(); 
                        $indexLoopArch++;
                        if($indexLoopArch > 1){
			              $cropsize = 180;
			            }
                        $post = get_post(get_the_ID());
                        $field = get_field('position', get_the_ID());
                        ?>
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
                            <div class="row">
                            <?php 
                            $textsize = 'col-md-7'; 
                            if (has_post_thumbnail( $post->ID ) ) {
                            $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
                            $thumbnailURL = $image_url[0];
                            $image = @aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
                            if(empty($image)) { $image = $thumbnailURL; } ?>
                                <div class="col-md-5">
                                    <div class="imghoverclass img-margin-center">
                                        <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
                                            <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
                                        </a> 
                                    </div>
                                </div>
                                <?php $image = null; $thumbnailURL = null; 
                            } else {
                                $thumbnailURL = virtue_post_default_placeholder();
                                $image = @aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
                                if(empty($image)) { $image = $thumbnailURL; } ?>
                                <div class="col-md-5">
                                    <div class="imghoverclass img-margin-center">
                                        <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
                                            <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
                                        </a> 
                                    </div>
                                </div>
                                <?php $image = null; $thumbnailURL = null; 
                            } ?>

                                <div class="<?php echo esc_attr($textsize);?> postcontent">
                                    <header>
                                        <a href="<?php the_permalink() ?>"><h2 class="entry-title" itemprop="name headline"><?php the_title(); ?></h2></a>
                                        <?php // get_template_part('templates/entry', 'meta-subhead'); ?>    
                                    </header>
                                    <div class="plus-read-more"><a href="<?php the_permalink() ?>">+</a></div>
                                </div><!-- postcontent -->

                                <div class="<?php echo esc_attr($textsize);?> postcontent-excerpt">
                                    <header>
                                        <a href="<?php the_permalink() ?>"><h2 class="entry-title" itemprop="name headline"><?php the_title(); ?></h2></a>
                                    </header>
                                    <div class="entry-content" itemprop="articleBody">
                                        <?php 
                                        # First Article is long desc
                                        if($indexLoopArch == 1) {
                                            add_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
                                            add_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
                                        }
                                        else {
                                            remove_filter('excerpt_more', 'kadence_excerpt_more');
                                            add_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
                                            add_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
                                        }
                                        the_excerpt();
                                        if($indexLoopArch == 1) {
                                            remove_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
                                            remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
                                        }else {
                                            remove_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
                                            remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
                                        }
                                        ?>
                                    </div>
                                    <div class="plus-read-more"><a href="<?php the_permalink() ?>">+</a></div>
                                </div><!-- postcontent-excerpt -->
                            </div><!-- row-->
                        </article> <!-- Article -->
                        <?php 
                    endwhile;
                    ?>
                        <div class="clearBoth"></div>
                </div>
            </div>
        </div>
    </div>
        
<?php        
    }else{
?>
        <div id="content" class="container">
            <div class="row row-archive">
                <div class="main col-lg-9 col-md-8 postlist main-archive" role="main">  
                    <article id="detail-cat">
                        <h2><?php echo $category_title ?></h2>
                        <?php 
                            if(isset($category_description) && !empty($category_description))
                            {
                                echo $category_description;
                            }                    
                        ?>
                        <a id="cat-link" target="_blank" href="<?php echo $linkCat ?>" title="<?php echo __('[:fr]Lien externe[:en]External Link') ?>">
                            <?php echo __('[:fr]Lien[:en]Link'); ?>
                        </a>
                    </article>
                    <!-- Position Forfait -->
                    <div id="slick-forfait" class="article-slider">
                        <?php                 
                        $indexLoopArch = 0;
                        $cropsize = 365;
                        
                        while (have_posts()) : the_post(); 
                            $indexLoopArch++;
                            if($indexLoopArch > 1){
                            	$cropsize = 180;
                            }
                            $post = get_post(get_the_ID());
                            $field = get_field('position', get_the_ID());
                            if($field == 'forfait'):
                                ?>
                                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
                                    <div class="row">
                                    <?php 
                                    $textsize = 'col-md-7'; 
                                    if (has_post_thumbnail( $post->ID ) ) {
                                    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
                                    $thumbnailURL = $image_url[0];
                                    $image = @aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
                                    if(empty($image)) { $image = $thumbnailURL; } ?>
                                        <div class="col-md-5">
                                            <div class="imghoverclass img-margin-center">
                                                <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
                                                    <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
                                                </a> 
                                            </div>
                                        </div>
                                        <?php $image = null; $thumbnailURL = null; 
                                    } else {
                                        $thumbnailURL = virtue_post_default_placeholder();
                                        $image = @aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
                                        if(empty($image)) { $image = $thumbnailURL; } ?>
                                        <div class="col-md-5">
                                            <div class="imghoverclass img-margin-center">
                                                <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
                                                    <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
                                                </a> 
                                            </div>
                                        </div>
                                        <?php $image = null; $thumbnailURL = null; 
                                    } ?>

                                        <div class="<?php echo esc_attr($textsize);?> postcontent">
                                            <header>
                                                <a href="<?php the_permalink() ?>"><h2 class="entry-title" itemprop="name headline"><?php the_title(); ?></h2></a>
                                                <?php // get_template_part('templates/entry', 'meta-subhead'); ?>    
                                            </header>
                                            <div class="plus-read-more"><a href="<?php the_permalink() ?>">+</a></div>
                                        </div><!-- postcontent -->

                                        <div class="<?php echo esc_attr($textsize);?> postcontent-excerpt">
                                            <header>
                                                <a href="<?php the_permalink() ?>"><h2 class="entry-title" itemprop="name headline"><?php the_title(); ?></h2></a>
                                            </header>
                                            <div class="entry-content" itemprop="articleBody">
                                                <?php 
                                                # First Article is long desc
                                                if($indexLoopArch == 1) {
                                                    add_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
                                                    add_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
                                                }
                                                else {
                                                    remove_filter('excerpt_more', 'kadence_excerpt_more');
                                                    add_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
                                                    add_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
                                                }
                                                the_excerpt();
                                                if($indexLoopArch == 1) {
                                                    remove_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
                                                    remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
                                                }else {
                                                    remove_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
                                                    remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
                                                }
                                                ?>
                                            </div>
                                            <div class="plus-read-more"><a href="<?php the_permalink() ?>">+</a></div>
                                        </div><!-- postcontent-excerpt -->
                                    </div><!-- row-->
                                </article> <!-- Article -->
                                <?php 
                            endif;
                        endwhile;
                        ?>
                    </div>
                    <!-- Position Information -->
                    <div id="slick-information" class="article-slider">
                        <?php 
                        $indexLoopArch = 0;
                        $cropsize = 365;
                        
                        while (have_posts()) : the_post(); 
                            $indexLoopArch++;
                            if($indexLoopArch > 1){
                            	$cropsize = 180;
                            }
                            $post = get_post(get_the_ID());
                            $field = get_field('position', get_the_ID());
                            if($field == 'information'):
                                ?>
                                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
                                    <div class="row">
                                    <?php 
                                    $textsize = 'col-md-7'; 
                                    if (has_post_thumbnail( $post->ID ) ) {
                                    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
                                    $thumbnailURL = $image_url[0];
                                    $image = @aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
                                    if(empty($image)) { $image = $thumbnailURL; } ?>
                                        <div class="col-md-5">
                                            <div class="imghoverclass img-margin-center">
                                                <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
                                                    <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
                                                </a> 
                                            </div>
                                        </div>
                                        <?php $image = null; $thumbnailURL = null; 
                                    } else {
                                        $thumbnailURL = virtue_post_default_placeholder();
                                        $image = @aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
                                        if(empty($image)) { $image = $thumbnailURL; } ?>
                                        <div class="col-md-5">
                                            <div class="imghoverclass img-margin-center">
                                                <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
                                                    <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
                                                </a> 
                                            </div>
                                        </div>
                                        <?php $image = null; $thumbnailURL = null; 
                                    } ?>

                                        <div class="<?php echo esc_attr($textsize);?> postcontent">
                                            <header>
                                                <a href="<?php the_permalink() ?>"><h2 class="entry-title" itemprop="name headline"><?php the_title(); ?></h2></a>
                                                <?php // get_template_part('templates/entry', 'meta-subhead'); ?>    
                                            </header>
                                            <div class="plus-read-more"><a href="<?php the_permalink() ?>">+</a></div>
                                        </div><!-- postcontent -->

                                        <div class="<?php echo esc_attr($textsize);?> postcontent-excerpt">
                                            <header>
                                                <a href="<?php the_permalink() ?>"><h2 class="entry-title" itemprop="name headline"><?php the_title(); ?></h2></a>
                                            </header>
                                            <div class="entry-content" itemprop="articleBody">
                                                <?php 
                                                # First Article is long desc
                                                if($indexLoopArch == 1) {
                                                    add_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
                                                    add_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
                                                }
                                                else {
                                                    remove_filter('excerpt_more', 'kadence_excerpt_more');
                                                    add_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
                                                    add_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
                                                }
                                                the_excerpt();
                                                if($indexLoopArch == 1) {
                                                    remove_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
                                                    remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
                                                }else {
                                                    remove_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
                                                    remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
                                                }
                                                ?>
                                            </div>
                                            <div class="plus-read-more"><a href="<?php the_permalink() ?>">+</a></div>
                                        </div><!-- postcontent-excerpt -->
                                    </div><!-- row-->
                                </article> <!-- Article -->
                                <?php 
                            endif;
                        endwhile;
                        ?>                
                    </div>
                    <!-- Position Row 1 -->
                    <div id="slick-row-1" class="article-slider new-row">
                        <?php 
                        $indexLoopArch = 0;
                        $cropsize = 365;
                        
                        while (have_posts()) : the_post(); 
                            $indexLoopArch++;
                            if($indexLoopArch > 1){
                            	$cropsize = 180;
                            }
                            $post = get_post(get_the_ID());
                            $field = get_field('position', get_the_ID());
                            if($field == 'row1'):
                                ?>
                                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
                                    <div class="row">
                                    <?php 
                                    $textsize = 'col-md-7'; 
                                    if (has_post_thumbnail( $post->ID ) ) {
                                    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
                                    $thumbnailURL = $image_url[0];
                                    $image = @aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
                                    if(empty($image)) { $image = $thumbnailURL; } ?>
                                        <div class="col-md-5">
                                            <div class="imghoverclass img-margin-center">
                                                <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
                                                    <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
                                                </a> 
                                            </div>
                                        </div>
                                        <?php $image = null; $thumbnailURL = null; 
                                    } else {
                                        $thumbnailURL = virtue_post_default_placeholder();
                                        $image = @aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
                                        if(empty($image)) { $image = $thumbnailURL; } ?>
                                        <div class="col-md-5">
                                            <div class="imghoverclass img-margin-center">
                                                <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
                                                    <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
                                                </a> 
                                            </div>
                                        </div>
                                        <?php $image = null; $thumbnailURL = null; 
                                    } ?>

                                        <div class="<?php echo esc_attr($textsize);?> postcontent">
                                            <header>
                                                <a href="<?php the_permalink() ?>"><h2 class="entry-title" itemprop="name headline"><?php the_title(); ?></h2></a>
                                                <?php // get_template_part('templates/entry', 'meta-subhead'); ?>    
                                            </header>
                                            <div class="plus-read-more"><a href="<?php the_permalink() ?>">+</a></div>
                                        </div><!-- postcontent -->

                                        <div class="<?php echo esc_attr($textsize);?> postcontent-excerpt">
                                            <header>
                                                <a href="<?php the_permalink() ?>"><h2 class="entry-title" itemprop="name headline"><?php the_title(); ?></h2></a>
                                            </header>
                                            <div class="entry-content" itemprop="articleBody">
                                                <?php 
                                                # First Article is long desc
                                                if($indexLoopArch == 1) {
                                                    add_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
                                                    add_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
                                                }
                                                else {
                                                    remove_filter('excerpt_more', 'kadence_excerpt_more');
                                                    add_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
                                                    add_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
                                                }
                                                the_excerpt();
                                                if($indexLoopArch == 1) {
                                                    remove_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
                                                    remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
                                                }else {
                                                    remove_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
                                                    remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
                                                }
                                                ?>
                                            </div>
                                            <div class="plus-read-more"><a href="<?php the_permalink() ?>">+</a></div>
                                        </div><!-- postcontent-excerpt -->
                                    </div><!-- row-->
                                </article> <!-- Article -->
                                <?php 
                            endif;
                        endwhile;
                        ?>                
                    </div>
                    <!-- Position Row 2 -->
                    <div id="slick-row-2" class="article-slider new-row">
                        <?php 
                        $indexLoopArch = 0;
                        $cropsize = 365;
                        
                        while (have_posts()) : the_post(); 
                            $indexLoopArch++;
                            if($indexLoopArch > 1){
                            	$cropsize = 180;
                            }
                            $post = get_post(get_the_ID());
                            $field = get_field('position', get_the_ID());
                            if($field == 'row2'):
                                ?>
                                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope="" itemtype="http://schema.org/BlogPosting">
                                    <div class="row">
                                    <?php 
                                    $textsize = 'col-md-7'; 
                                    if (has_post_thumbnail( $post->ID ) ) {
                                    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
                                    $thumbnailURL = $image_url[0];
                                    $image = @aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
                                    if(empty($image)) { $image = $thumbnailURL; } ?>
                                        <div class="col-md-5">
                                            <div class="imghoverclass img-margin-center">
                                                <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
                                                    <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
                                                </a> 
                                            </div>
                                        </div>
                                        <?php $image = null; $thumbnailURL = null; 
                                    } else {
                                        $thumbnailURL = virtue_post_default_placeholder();
                                        $image = @aq_resize($thumbnailURL, $cropsize, $cropsize, true, true, true);
                                        if(empty($image)) { $image = $thumbnailURL; } ?>
                                        <div class="col-md-5">
                                            <div class="imghoverclass img-margin-center">
                                                <a href="<?php the_permalink()  ?>" title="<?php the_title(); ?>">
                                                    <img src="<?php echo esc_url($image); ?>" alt="<?php the_title(); ?>" class="iconhover" style="display:block;">
                                                </a> 
                                            </div>
                                        </div>
                                        <?php $image = null; $thumbnailURL = null; 
                                    } ?>

                                        <div class="<?php echo esc_attr($textsize);?> postcontent">
                                            <header>
                                                <a href="<?php the_permalink() ?>"><h2 class="entry-title" itemprop="name headline"><?php the_title(); ?></h2></a>
                                                <?php // get_template_part('templates/entry', 'meta-subhead'); ?>    
                                            </header>
                                            <div class="plus-read-more"><a href="<?php the_permalink() ?>">+</a></div>
                                        </div><!-- postcontent -->

                                        <div class="<?php echo esc_attr($textsize);?> postcontent-excerpt">
                                            <header>
                                                <a href="<?php the_permalink() ?>"><h2 class="entry-title" itemprop="name headline"><?php the_title(); ?></h2></a>
                                            </header>
                                            <div class="entry-content" itemprop="articleBody">
                                                <?php 
                                                # First Article is long desc
                                                if($indexLoopArch == 1) {
                                                    add_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
                                                    add_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
                                                }
                                                else {
                                                    remove_filter('excerpt_more', 'kadence_excerpt_more');
                                                    add_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
                                                    add_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
                                                }
                                                the_excerpt();
                                                if($indexLoopArch == 1) {
                                                    remove_filter( 'excerpt_length', 'custom_excerpt_length_long', 99 );
                                                    remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_long', 99 );
                                                }else {
                                                    remove_filter( 'excerpt_length', 'custom_excerpt_length_short', 99 );
                                                    remove_filter( 'the_excerpt', 'custom_cut_excerpt_length_short', 99 );
                                                }
                                                ?>
                                            </div>
                                            <div class="plus-read-more"><a href="<?php the_permalink() ?>">+</a></div>
                                        </div><!-- postcontent-excerpt -->
                                    </div><!-- row-->
                                </article> <!-- Article -->
                                <?php 
                            endif;
                        endwhile;
                        ?>
                    </div>

                </div>
            </div>
        </div>
        <?php /*
        <script type="text/javascript" src="<?php echo get_site_url() ?>/wp-content/themes/charmey/slider/js/slick.min.js"></script>
        <script type="text/javascript">
            window.$ = jQuery
            $(document).ready(function(){         	
                var slidesToShow = 1;
                var width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
                if(width <= 750)
                {
                    alert('Small Screen');
                    $('#slick-forfait').slick({
                        autoplay: true,
                        autoplaySpeed: 3000,
                        speed: 1000,
                        slidesToShow: 2,
                        slidesToScroll: 2
                    });
                    $('#slick-information').slick({
                        autoplay: true,
                        autoplaySpeed: 3050,
                        speed: 1000,
                        slidesToShow: 2,
                        slidesToScroll: 2
                    });
                    $('#slick-row-1').slick({
                        autoplay: true,
                        autoplaySpeed: 3100,
                        speed: 1000,
                        slidesToShow: 2,
                        slidesToScroll: 2
                    });
                    $('#slick-row-2').slick({
                        autoplay: true,
                        autoplaySpeed: 3150,
                        speed: 1000,
                        slidesToShow: 2,
                        slidesToScroll: 2
                    });
                }else{
                    $('#slick-forfait').slick({
                        autoplay: true,
                        autoplaySpeed: 3000,
                        speed: 1000,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    });
                    $('#slick-information').slick({
                        autoplay: true,
                        autoplaySpeed: 3050,
                        speed: 1000,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    });
                    $('#slick-row-1').slick({
                        autoplay: true,
                        autoplaySpeed: 3100,
                        speed: 1000,
                        slidesToShow: 3,
                        slidesToScroll: 1
                    });
                    $('#slick-row-2').slick({
                        autoplay: true,
                        autoplaySpeed: 3150,
                        speed: 1000,
                        slidesToShow: 3,
                        slidesToScroll: 1
                    });
                }        
            });
        </script>
        */ ?>
    <?php } ?>